<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3 class="box-title m-b-0">Members </h3>
                    <p class="text-muted m-b-20">All members</p>
                </div>    
                <div class="col-md-6">
                    <a href="<?php echo base_url('admin/members/add') ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add Member</a>
                </div>    
            </div>
            
            <div class="table-responsive">
                <table class="table" id="memberTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Member</th>
                            <th>Membership Type</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Renewal Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>