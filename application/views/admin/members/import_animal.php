
<!-- Start Page Content -->

<div class="row">
  <div class="col-lg-12">


   <div class="panel panel-info">
    <div class="panel-heading"> 
     <i class="fa fa-plus"></i> &nbsp;Import Animal 

   </div>
   <div class="panel-body table-responsive">

     <?php $error_msg = $this->session->flashdata('error_msg'); ?>
     <?php if (isset($error_msg)): ?>
      <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
      </div>
    <?php endif ?>


    <form id="validateForm" method="post" action="<?php echo base_url('admin/member/import_animal_csv') ?>" class="form-horizontal form-material" enctype="multipart/form-data">
    <div class="form-group">
      <label class="col-md-12" for="example-text">Upload CSV</label>
      <div class="col-sm-12">
        <input type="file" name="import_animal" class="form-control" value="" required="">
      </div>
    </div>

    
    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
        
     <div class="form-group col-md-12">
      <button type="submit" class="btn btn-info btn-rounded"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Import</button>
     </div>

  </div>

</form>
</div>
</div>
</div>
</div>

    <!-- End Page Content -->