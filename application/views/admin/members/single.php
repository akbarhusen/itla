<div class="row">
    <div class="col-md-10">
        <div class="panel panel-success">
            <div class="panel-heading">Member Info
                <a href="<?php echo base_url('admin/members') ?>" class="btn btn-sm pull-right"><i class="fa fa-list"></i>&nbsp;All Members</a></div>
            <div class="panel-body">
                <div class="media">
                    <div class="media-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <?php if(!empty($member->brand)): ?>
                                    <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="<?php echo base_url()."uploads/animals/".$member->brand ?>" data-holder-rendered="true" style="height: 64px;">
                                <?php endif; ?>
                                <h4 class="media-heading"><?php echo ucfirst($member->member) ?></h4> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label class="control-label">Membership Type: </label> <?php echo ucfirst($member->membership_type) ?><br>
                                <label class="control-label">Address: </label> <?php echo ucfirst($member->address) ?><br>
                                <label class="control-label">Email: </label> <?php echo ucfirst($member->email) ?><br>
                                <label class="control-label">Ranch: </label> <?php echo ucfirst($member->ranch) ?><br>
                                <label class="control-label">Partnership: </label> <?php echo ($member->partnership == 1) ? 'Yes' : 'No' ?><br>
                                <label class="control-label">Address Type: </label> <?php echo ucfirst($member->address_type) ?><br>
                                <label class="control-label">Region: </label> <?php echo ucfirst($member->region) ?><br>
                                <label class="control-label">Mailing Address Type: </label> <?php echo ($member->mailing_address_type == 1) ? 'Same as physical address' : '' ?><br>
                                        
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Website: </label> <?php echo ucfirst($member->website) ?><br>
                                <label class="control-label">Phone: </label> <?php echo ucfirst($member->phone) ?><br>
                                <label class="control-label">Date Joined: </label> <?php echo ucfirst($member->date_joined) ?><br>
                                <label class="control-label">Expiration Date: </label> <?php echo ucfirst($member->expiration_date) ?><br>
                                <label class="control-label">Birth Date: </label> <?php echo ucfirst($member->birth_date) ?><br>
                                <label class="control-label">Opt In: </label> <?php echo ($member->opt_in == 1) ? 'Yes' : 'No' ?><br>
                                <label class="control-label">Renewal Date: </label> <?php echo ucfirst($member->renewal_date) ?><br>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>