
<!-- Start Page Content -->

<div class="row">
  <div class="col-lg-12">


   <div class="panel panel-info">
    <div class="panel-heading"> 
     <i class="fa fa-pencil"></i> &nbsp; Edit Member <a href="<?php echo base_url('admin/members') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> List members </a>

   </div>
   <div class="panel-body table-responsive">

     <?php $error_msg = $this->session->flashdata('error_msg'); ?>
     <?php if (isset($error_msg)): ?>
      <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
      </div>
    <?php endif ?>


    <form id="validateForm" method="post" action="<?php echo base_url('admin/member/update') ?>" class="form-horizontal form-material" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $member->id ?>">
    <div class="form-group">
      <label class="col-md-12" for="example-text">Customer Name</label>
      <div class="col-sm-12">
        <input type="text" name="member" class="form-control" value="<?php echo $member->member ?>" required="">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Ranch Name</label>
      <div class="col-sm-12">
        <input type="text" name="ranch" class="form-control" value="<?php echo $member->ranch ?>" required="">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Membership Type</label>
      <div class="col-sm-12">
        <select class="form-control" name="membership_type" required="">
          <option value="">Select</option>
          <option <?php echo ($member->membership_type == 'Active') ? 'selected' : '' ?> value="Active">Active</option>
          <option <?php echo ($member->membership_type == 'Inactive') ? 'selected' : '' ?> value="Inactive">Inactive</option>
          <option <?php echo ($member->membership_type == 'Honorary') ? 'selected' : '' ?> value="Honorary">Honorary</option>
          <option <?php echo ($member->membership_type == 'Lifetime') ? 'selected' : '' ?> value="Lifetime">Lifetime</option>
          <option <?php echo ($member->membership_type == 'Non Member') ? 'selected' : '' ?> value="Non Member">Non Member</option>
          <option <?php echo ($member->membership_type == 'Youth') ? 'selected' : '' ?> value="Youth">Youth</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-12">
        <input type="checkbox" class="check" <?php echo ($member->partnership == 1) ? 'checked' : '' ?> id="partnership" name="partnership" data-checkbox="icheckbox_square-green" value="1">
        <label for="partnership"> In Partnership</label>
      </div>
    </div>


    <div class="form-group">
      <label class="col-md-12" for="example-text">Physical Address</label>
      <div class="col-sm-12">
        <input type="radio" class="check" id="domestic" name="address_type" <?php echo ($member->address_type == 'domestic') ? 'checked' : '' ?> data-radio="iradio_flat-green" value="domestic">
        <label for="domestic">Domestic Address</label>
        <input type="radio" class="check" id="foreign" name="address_type" <?php echo ($member->address_type == 'foreign') ? 'checked' : '' ?> data-radio="iradio_flat-green" value="foreign">
        <label for="foreign">Foreign Address</label>
      </div>
      <br>
      <div class="col-sm-12">
        <textarea name="address" class="form-control"><?php echo $member->address ?></textarea>
      </div>
    </div>


    <div class="form-group">
      <label class="col-md-12" for="example-text">Region</label>
      <div class="col-sm-12">
        <input type="text" name="region" class="form-control" value="<?php echo $member->region ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Mailing Address</label>
      <div class="col-sm-12">
        <input type="checkbox" class="check" id="mailing_address_type" name="mailing_address_type" data-checkbox="icheckbox_square-green" <?php echo ($member->mailing_address_type == 1) ? 'checked' : '' ?> value="1">
        <label for="mailing_address_type">Same As Physical Address</label>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Ranch Email</label>
      <div class="col-sm-12">
        <input type="email" name="email" class="form-control" value="<?php echo $member->email ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Website</label>
      <div class="col-sm-12">
        <input type="text" name="website" class="form-control" value="<?php echo $member->website ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Phone</label>
      <div class="col-sm-12">
        <input type="text" name="phone" class="form-control" value="<?php echo $member->phone ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Date Joined</label>
      <div class="col-sm-12">
        <input type="text" name="date_joined" class="form-control mydatepicker" value="<?php echo $member->date_joined ?>" required autocomplete="off">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Expiration Date</label>
      <div class="col-sm-12">
        <input type="text" name="expiration_date" class="form-control mydatepicker" value="<?php echo $member->expiration_date ?>" required autocomplete="off">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-12" for="example-text">Youth member Birth Date</label>
      <div class="col-sm-12">
        <input type="text" name="birth_date" class="form-control mydatepicker" value="<?php echo $member->birth_date ?>" required autocomplete="off">
      </div>
    </div>    

    <div class="form-group">
      <input type="checkbox" class="check" id="opt_in" name="opt_in" <?php echo ($member->opt_in == 1) ? 'checked' : '' ?> data-checkbox="icheckbox_square-green" value="1">
        <label for="opt_in">Opt In</label>
    </div>

    <div class="form-group col-md-12">
        <label class="col-md-12">Brand</label>
        <div class="col-md-12">
            <input type="file" class="dropify" name="image" data-height="300" data-default-file="<?php echo base_url()."uploads/animals/".$member->brand ?>" />
        </div>
    </div>

    <div class="form-group col-md-12">
        <label class="col-md-12">Renewal Date</label>
        <div class="col-md-12">
            <input type="text" name="renewal_date" value="<?php echo $member->renewal_date ?>" class="form-control mydatepicker" required autocomplete="off">
        </div>
    </div>
    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
     <hr>   
     <div class="form-group col-md-12">
      <button type="submit" class="btn btn-info btn-rounded"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
     </div>

  </div>

</form>
</div>
</div>
</div>
</div>

    <!-- End Page Content -->