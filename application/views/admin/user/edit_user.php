
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">

            
           <div class="panel panel-info">
                <div class="panel-heading"> 
                     <i class="fa fa-pencil"></i> &nbsp;User Edit <a href="<?php echo base_url('admin/user/all_user_list') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> All Users </a>

                </div>
                <div class="panel-body table-responsive">
				
				 <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">�</span> </button>
                </div>
            <?php endif ?>
			
			
                    <form method="post" action="<?php echo base_url('admin/user/update/'.$user->id) ?>" class="form-horizontal" novalidate>
                        <div class="form-group">
                        <label class="col-md-12" for="example-text">Username: <?php echo $user->username ?></label>
                        </div>
                       <div class="form-group">
                     	<label class="col-md-12" for="example-text">First Name</label>
                            <div class="col-sm-12">
                                <input type="text" name="first_name" class="form-control" value="<?php echo $user->first_name; ?>">
                            </div>
                        </div>
                              

                           <div class="form-group">
                 	<label class="col-md-12" for="example-text">Last Name</label>
                    <div class="col-sm-12">
                     <input type="text" name="last_name" class="form-control" value="<?php echo $user->last_name; ?>">
                                        </div>
                                    </div>
                              

                           <div class="form-group">
                 	<label class="col-md-12" for="example-text">Email</label>
                    <div class="col-sm-12">
                   <input type="text" name="email" class="form-control" value="<?php echo $user->email; ?>">
                                        </div>
                                    </div>
                              

                        <div class="form-group">
                        <label class="col-md-12" for="example-text">Membership : <?php echo $user->membership ?></label>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-12">
                            <input type="checkbox" class="check" <?php echo ($user->locked == 1) ? 'checked' : '' ?> id="locked" name="locked" data-checkbox="icheckbox_square-green" value="1">
                            <label for="locked"> Locked</label>
                          </div>
                        </div>
                        <?php if(empty($user->member_id)): ?>
                        <div class="form-group">
                            <label class="col-md-12" for="example-text">Member</label>
                            <div class="col-sm-12">
                                <input type="text" name="member" id="member" class="form-control">
                                <input type="hidden" name="member_email" id="member_email" value="">
                            </div>
                        </div>
                        <?php else: ?>
                            <div class="form-group">
                                <label class="col-md-12" for="example-text">Member Linked : <?php $this->load->model('common_model'); echo $this->common_model->getOne($user->member_id, 'members')->member ?></label>
                            </div>
                            <input type="hidden" name="member" id="member" class="form-control">
                        <?php endif; ?>
									
									
                           
						
                                    
                         <!--  New User <input <?php if($user->role == "user"){echo "checked";}; ?> type="radio" onclick="javascript:yesnoCheck2();" name="role" id="yesCheck" value="user"> 
						  New Admin<input <?php if($user->role == "admin"){echo "checked";}; ?> type="radio" onclick="javascript:yesnoCheck2();" name="role" id="noCheck" value="admin"><br>
    					  <div id="ifYes2"> -->
						  <hr>
                          
                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
  <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info btn-rounded btn-sm"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
                            </div>
                        </div>
                           
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Content -->