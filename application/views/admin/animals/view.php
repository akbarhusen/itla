<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3 class="box-title m-b-0">Animals </h3>
                    <p class="text-muted m-b-20">Animals you created </p>
                </div>    
                <div class="col-md-6">
                    <!-- <a href="<?php //echo base_url('user/my-animals/create/normal') ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create</a> -->
                    <a href="<?php echo base_url('register-animal') ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Register Animal</a> 

                    <a href="<?php echo base_url('admin/animals/export/csv') ?>" class="btn btn-warning pull-right"><i class="fa fa-file-excel-o"></i> Export Animals</a>        
                </div>    
            </div>

            <div class="table-responsive">
                <table class="table" id="animalsList">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>DOB</th>
                            <th>Sex</th>
                            <th>Sire</th>
                            <th>Dam</th>
                            <th>ITLA</th>
                            <th>TLBA Number</th>
                            <th>PH #</th>
                            <th>PH Location</th>
                            <th>Owner/Breeder</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>