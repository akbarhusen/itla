 <style type="text/css">
 	.white-box .box-title {
 		margin: 0px;
 	}
 	.pedigree-tree{width:100%;overflow-x:scroll;overflow-y:hidden;white-space:nowrap;position:relative;font-family:sans-serif}.pedigree-tree ul{padding-left:0;transition:all .5s;-webkit-transition:all .5s;-moz-transition:all .5s;position:relative;margin-left:250px!important}.pedigree-tree ul:before{content:"";width:50px;border-top:2px solid #EEE;position:absolute;left:-100px;top:50%;margin-top:1px;transition:all .5s;-webkit-transition:all .5s;-moz-transition:all .5s}.pedigree-tree > ul:nth-child(1){margin-left:0!important}.pedigree-tree > ul:nth-child(1):before{border:0}.pedigree-tree li{list-style:none;position:relative;min-height:75px;transition:all .5s;-webkit-transition:all .5s;-moz-transition:all .5s}.pedigree-tree li:before{content:"";height:100%;border-left:2px solid #EEE;position:absolute;left:-50px;transition:all .5s;-webkit-transition:all .5s;-moz-transition:all .5s}.pedigree-tree li:after{content:"";width:50px;border-top:2px solid #EEE;position:absolute;left:-50px;top:50%;margin-top:1px;transition:all .5s;-webkit-transition:all .5s;-moz-transition:all .5s}.pedigree-tree li:first-child:before{width:10px;height:50%;top:50%;margin-top:2px;border-radius:10px 0 0 0}.pedigree-tree li:first-child:after{height:10px;border-radius:10px 0 0 0}.pedigree-tree li:last-child:before{width:10px;height:50%;border-radius:0 0 0 10px}.pedigree-tree li:last-child:after{height:10px;border-top:none;border-bottom:2px solid #EEE;border-radius:0 0 0 10px;margin-top:-9px}.pedigree-tree li.sole:before{display:none}.pedigree-tree li.sole:after{width:50px;height:0;margin-top:1px;border-radius:0}.pedigree-tree span.wr{display:block;width:150px;position:absolute;left:0;top:50%;margin-top:-30px;font-size:.8em;color:#666;background-color:#e2e0e0;background-repeat:no-repeat;background-position:center center;background-size:cover;transition:all .5s;-webkit-transition:all .5s;-moz-transition:all .5s}.pedigree-tree li.female > span.wr{border-color:rgba(255,105,180,1)}.pedigree-tree li.male > span.wr{border-color:rgba(105,180,255,1)}.pedigree-tree span.wr a{display:block;white-space:pre-wrap;border:0;padding:1em;text-decoration:none}.pedigree-tree span.wr a time{opacity:.7;font-size:.7em}.pedigree-tree span.inexistant{min-height:80px;background-color:rgba(0,0,0,.2)}.pedigree-tree li > span.wr:not(.nobg):hover a,.pedigree-tree li > span.wr:not(.nobg):hover a time{background-color:transparent;color:transparent}.pedigree-tree li > span.wr:hover a.private{cursor:not-allowed}

  .pedigree-tree li.female > span a, .pedigree-tree span.wr a time {
    color: #FFFFFF;
  }

  .pedigree-tree li.female > span a {
    background-color: rgba(232,37,215,0.7);
  }
  .pedigree-tree li.male > span a, .pedigree-tree span.wr a time {
    color: #FFFFFF;
  }

  .pedigree-tree li.male > span a {
    background-color: rgba(30,115,190,0.7);
  }
 </style>
 <div class="row">
	    <div class="col-sm-12">
	        <div class="white-box p-l-20 p-r-20">
                <?php if ($this->session->userdata('role') == 'admin'): ?>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            
                        </div>    
                        <div class="col-md-6">
                            <a href="<?php echo base_url('user/animal/edit/'.$animal->id)
                             ?>" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i> Edit Animal</a>
                             <?php if($animal->status == 0): ?>
                                <a href="<?php echo base_url('user/my-animals/update-status/reject/'.$animal->id)
                                 ?>" class="btn btn-danger pull-right"><i class="fa fa-trash"></i> Reject</a>
                                 <a href="<?php echo base_url('user/my-animals/update-status/approve/'.$animal->id) ?>" class="btn btn-success pull-right"><i class="fa fa-check"></i> Approve</a>
                         <?php endif; ?>
                        </div>    
                    </div>
                <?php endif; ?>
	            <div class="row">
                    <div class="col-md-12">
                    	<div class="col-md-12 el-card-content">
                            <h2><?php echo ucfirst($animal->name); ?></h2>
                        </div>
                    	<div class="col-md-6 el-element-overlay">
                    		<div class="el-card-item">

                                <div class="el-card-avatar el-overlay-1">
                                    <img src="<?php echo $image ?>">
                                    <div class="el-overlay">
                                        <ul class="el-info">
                                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo $image ?>"><i class="icon-magnifier"></i></a></li>
                                            <!-- <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>

                                <div class="el-card-content">
	                                <h3 class="box-title"><label class="control-label">Service:</label> <?php echo $animal->service ?></h3>
	                                <h3 class="box-title"><label class="control-label">PH #:</label> <?php echo $animal->private_herd ?></h3>
	                                <h3 class="box-title"><label class="control-label">Millennium Futurity:</label> <?php echo $animal->millennium_futurity ?></h3>
	                                <h3 class="box-title"><label class="control-label">Birth Weight:</label> <?php echo $animal->birth_weight ?></h3>
	                                <h3 class="box-title"><label class="control-label">Status:</label> <?php echo ($animal->status == 1) ? 'Active' : 'Not Active'; ?></h3>
	                                <h3 class="box-title"><label class="control-label">Brand Location:</label> <?php echo $animal->brand_location ?></h3>
	                            </div>
                                
                            </div>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="el-card-content">
                                <h3 class="box-title"><label class="control-label">Registration Date:</label> <?php echo date('d/m/Y', strtotime($animal->created_at)) ?></h3>
                                <h3 class="box-title"><label class="control-label">Name:</label> <?php echo ucfirst($animal->name) ?></h3>
                                <h3 class="box-title"><label class="control-label">ITLA #:</label> <?php echo $animal->id ?></h3>
                                <h3 class="box-title"><label class="control-label">TLBA #:</label> <?php echo $animal->tlba_number ?></h3>
                                <h3 class="box-title"><label class="control-label">Sire:</label> <?php echo !empty($animal->sire_name) ? $animal->sire_name : 'Not Available'; ?></h3>
                                <h3 class="box-title"><label class="control-label">Dam:</label> <?php echo !empty($animal->dam_name) ? $animal->dam_name : 'Not Available'; ?></h3>
                                <h3 class="box-title"><label class="control-label">DOB:</label> <?php echo $animal->dob; ?></h3>
                                <h3 class="box-title"><label class="control-label">Sex:</label> <?php echo ($animal->sex == 1) ? 'Bull' : 'Cow'; ?></h3>
                                <h3 class="box-title"><label class="control-label">Original Owner:</label> <?php echo !empty($animal->user_id) ? $this->common_model->getOne($animal->user_id, 'user')->first_name : 'N/A'; ?></h3>
                                <h3 class="box-title"><label class="control-label">Original Ownership Date:</label> <?php echo $animal->original_ownership_date ?></h3>
                                <h3 class="box-title"><label class="control-label">Region:</label> <?php echo $animal->religion ?></h3>
                                <h3 class="box-title"><label class="control-label">Breeder:</label> <?php echo $animal->breeder ?></h3>
                                <h3 class="box-title"><label class="control-label">Color:</label> <?php echo $animal->color ?></h3>
                                <h3 class="box-title"><label class="control-label">OCV #:</label> <?php echo $animal->ocv ?></h3>
                                <h3 class="box-title"><label class="control-label">PH Location:</label> <?php echo $animal->private_herd_location ?></h3>
                                <h3 class="box-title"><label class="control-label">Twin:</label> <?php echo $animal->twin ?></h3>
                                <h3 class="box-title"><label class="control-label">Disposal Date:</label> <?php echo $animal->disposal_date ?></h3>
                                <h3 class="box-title"><label class="control-label">Brand:</label> <?php echo $animal->brand ?> </h3>
                                <h3 class="box-title"><label class="control-label">Type:</label> <?php echo $animal->type ?> </h3>
                                <?php if($animal->type == 'transfer-into-inventory'): ?>
                                    <h3 class="box-title"><label class="control-label">Seller Name:</label> <?php echo $animal->seller_name ?> </h3>
                                    <h3 class="box-title"><label class="control-label">Seller Phone:</label> <?php echo $animal->seller_phone ?> </h3>
                                <?php endif; ?>

                                <?php if($animal->type == 'transfer-out-inventory'): ?>
                                    <h3 class="box-title"><label class="control-label">Buyer Name:</label> <?php echo $animal->buyer_name ?> </h3>
                                    <h3 class="box-title"><label class="control-label">Buyer Phone:</label> <?php echo $animal->buyer_phone ?> </h3>
                                <?php endif; ?>
                            </div>
                    	</div>
                    </div>

                    <div class="col-md-12">
                    	<div class="col-md-12 el-card-content">
                    		<h2 class="uk-button uk-button-danger custom-uk-buttons">PEDIGREE</h2>	
                    	</div>
                    	<div class="col-md-12 el-card-content">
                    		<div class="breeder-section ranch-pedigree">
								<div class="pedigree-tree clear">
									<ul>
										<li class="<?php if(!$animal_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>">
											<span class="wr <?php echo (empty($animalSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo (!empty($animalSirInfo->image)) ? $animalSirInfo->image : ""; ?>');"><a href="<?php echo (!empty($animalSirInfo->id)) ? $animalSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirInfo->name) ? ucfirst($animalSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirInfo->dob) ? $animalSirInfo->dob : '' ?></time></a></span>
											<ul>
												<li class="<?php if(!$animal_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>">
													<span class="wr <?php echo (!empty($animalSirSirInfo) && empty($animalSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirInfo->image) ? base_url()."uploads/animals/".$animalSirSirInfo->image : ''; ?>');">
														<a href="<?php echo (!empty($animalSirSirInfo->id)) ? $animalSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirInfo->name) ? ucfirst($animalSirSirInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalSirSirInfo->dob) ? $animalSirSirInfo->dob : '' ?> </time></a></span>
														<ul>
															<li class="<?php if(!$animal_sir_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalSirSirSirInfo) && empty($animalSirSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirSirInfo->image) ? base_url()."uploads/animals/".$animalSirSirSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirSirSirInfo->id)) ? $animalSirSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirSirInfo->name) ?  ucfirst($animalSirSirSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirSirSirInfo->dob) ?> </time></a></span>
																<ul>
																	<li class="<?php if(!$animal_sir_sir_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalSirSirSirSirInfo) && empty($animalSirSirSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirSirSirInfo->image) ? base_url()."uploads/animals/".$animalSirSirSirSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirSirSirSirInfo->id)) ? $animalSirSirSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirSirSirInfo->name) ?  ucfirst($animalSirSirSirSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirSirSirSirInfo->dob) ?> </time></a></span></li>

																	<li class="<?php if(!$animal_sir_sir_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalSirSirSirDamInfo) && empty($animalSirSirSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirSirDamInfo->image) ? base_url()."uploads/animals/".$animalSirSirSirDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirSirSirDamInfo->id)) ? $animalSirSirSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirSirDamInfo->name) ? ucfirst($animalSirSirSirDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirSirSirDamInfo->dob) ? $animalSirSirSirDamInfo->dob : '' ?> </time></a></span></li>
																</ul>
														</li>

														<li class="<?php if(!$animal_sir_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalSirSirDamInfo) && empty($animalSirSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirDamInfo->image) ? base_url()."uploads/animals/".$animalSirSirDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirSirDamInfo->id)) ? $animalSirSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirDamInfo->name) ? ucfirst($animalSirSirDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirSirDamInfo->dob) ? $animalSirSirDamInfo->dob : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_sir_sir_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalSirSirDamSirInfo) && empty($animalSirSirDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirDamSirInfo->image) ? base_url()."uploads/animals/".$animalSirSirDamSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirSirDamSirInfo->id)) ? $animalSirSirDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirDamSirInfo->name) ?  ucfirst($animalSirSirDamSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirSirDamSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_sir_sir_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalSirSirDamDamInfo) && empty($animalSirSirDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirSirDamDamInfo->image) ? base_url()."uploads/animals/".$animalSirSirDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirSirDamDamInfo->id)) ? $animalSirSirDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirSirDamDamInfo->name) ? ucfirst($animalSirSirDamDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirSirDamDamInfo->dob) ? $animalSirSirDamDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>
													</ul>
												</li>
												<li class="<?php if(!$animal_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>">
													<span class="wr <?php echo (empty($animalSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamInfo->image) ? base_url()."uploads/animals/".$animalSirDamInfo->image : ''; ?> ?>');"><a href="<?php echo (!empty($animalSirDamInfo->id)) ? $animalSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamInfo->name) ? ucfirst($animalSirDamInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalSirDamInfo->dob) ? $animalSirDamInfo->dob : '' ?> </time></a></span>
													<ul>
														<li class="<?php if(!$animal_sir_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalSirDamSirInfo) && empty($animalSirDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamSirInfo->image) ? base_url()."uploads/animals/".$animalSirDamSirInfo->image : ''; ?> ?>');"><a href="<?php echo (!empty($animalSirDamSirInfo->id)) ? $animalSirDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamSirInfo->name) ? ucfirst($animalSirDamSirInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalSirDamSirInfo->dob) ? $animalSirDamSirInfo->dob : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_sir_dam_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalSirDamSirSirInfo) && empty($animalSirDamSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamSirSirInfo->image) ? base_url()."uploads/animals/".$animalSirDamSirSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirDamSirSirInfo->id)) ? $animalSirDamSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamSirSirInfo->name) ?  ucfirst($animalSirDamSirSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirDamSirSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_sir_dam_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalSirDamSirDamInfo) && empty($animalSirDamSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamSirDamInfo->image) ? base_url()."uploads/animals/".$animalSirDamSirDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirDamSirDamInfo->id)) ? $animalSirDamSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamSirDamInfo->name) ? ucfirst($animalSirDamSirDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirDamSirDamInfo->dob) ? $animalSirDamSirDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>

														<li class="<?php if(!$animal_sir_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalSirDamDamInfo) && empty($animalSirDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamDamInfo->image) ? base_url()."uploads/animals/".$animalSirDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirDamDamInfo->id)) ? $animalSirDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamDamInfo->image) ? ucfirst($animalSirDamDamInfo->image) : '' ?><br><time class="dob"><?php echo !empty($animalSirDamDamInfo->dob) ? $animalSirDamDamInfo->dob : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_sir_dam_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalSirDamDamSirInfo) && empty($animalSirDamDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamDamSirInfo->image) ? base_url()."uploads/animals/".$animalSirDamDamSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirDamDamSirInfo->id)) ? $animalSirDamDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamDamSirInfo->name) ?  ucfirst($animalSirDamDamSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirDamDamSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_sir_dam_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalSirDamDamDamInfo) && empty($animalSirDamDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalSirDamDamDamInfo->image) ? base_url()."uploads/animals/".$animalSirDamDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalSirDamDamDamInfo->id)) ? $animalSirDamDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalSirDamDamDamInfo->name) ? ucfirst($animalSirDamDamDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalSirDamDamDamInfo->dob) ? $animalSirDamDamDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>
													</ul>
												</li>
											</ul>
										</li>
										<li class="<?php if(!$animal_dam_status) { echo 'no-parent'; } else { echo 'male'; } ?>">
											<span class="wr <?php echo (!empty($animalDamInfo) && empty($animalDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamInfo->image) ? base_url()."uploads/animals/".$animalDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamInfo->id)) ? $animalDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamInfo->name) ? ucfirst($animalDamInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalDamInfo->dob) ? $animalDamInfo->dob : ''; ?> </time></a></span>
											<ul>
												<li class="<?php if(!$animal_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>">
													<span class="wr <?php echo (!empty($animalDamSirInfo) && empty($animalDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirInfo->image) ? base_url()."uploads/animals/".$animalDamSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirInfo->id)) ? $animalDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirInfo->name) ? ucfirst($animalDamSirInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalDamSirInfo->dob) ? $animalDamSirInfo->dob : '' ?> </time></a></span>
													<ul>
														<li class="<?php if(!$animal_dam_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalDamSirSirInfo) && empty($animalDamSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirSirInfo->image) ? base_url()."uploads/animals/".$animalDamSirSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirSirInfo->id)) ? $animalDamSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirSirInfo) ?  ucfirst($animalDamSirSirInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalDamSirSirInfo) ? $animalDamSirSirInfo->dob : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_dam_sir_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalDamSirSirSirInfo) && empty($animalDamSirSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirSirSirInfo->image) ? base_url()."uploads/animals/".$animalDamSirSirSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirSirSirInfo->id)) ? $animalDamSirSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirSirSirInfo->name) ?  ucfirst($animalDamSirSirSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamSirSirSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_dam_sir_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalDamSirSirDamInfo) && empty($animalDamSirSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirSirDamInfo->image) ? base_url()."uploads/animals/".$animalDamSirSirDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirSirDamInfo->id)) ? $animalDamSirSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirSirDamInfo->name) ? ucfirst($animalDamSirSirDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamSirSirDamInfo->dob) ? $animalDamSirSirDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>

														<li class="<?php if(!$animal_dam_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalDamSirDamInfo) && empty($animalDamSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirDamInfo->image) ? base_url()."uploads/animals/".$animalDamSirDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirDamInfo->id)) ? $animalDamSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirDamInfo) ?  ucfirst($animalDamSirDamInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalDamSirDamInfo) ? ucfirst($animalDamSirDamInfo->dob) : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_dam_sir_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalDamSirDamSirInfo) && empty($animalDamSirDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirDamSirInfo->image) ? base_url()."uploads/animals/".$animalDamSirDamSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirDamSirInfo->id)) ? $animalDamSirDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirDamSirInfo->name) ?  ucfirst($animalDamSirDamSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamSirDamSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_dam_sir_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalDamSirDamDamInfo) && empty($animalDamSirDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamSirDamDamInfo->image) ? base_url()."uploads/animals/".$animalDamSirDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamSirDamDamInfo->id)) ? $animalDamSirDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamSirDamDamInfo->name) ? ucfirst($animalDamSirDamDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamSirDamDamInfo->dob) ? $animalDamSirDamDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>
													</ul>
												</li>
												<li class="<?php if(!$animal_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>">
													<span class="wr <?php echo (empty($animalDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamInfo) ? $animalDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamInfo->id)) ? $animalDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamInfo->name) ? ucfirst($animalDamDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamDamInfo->dob) ? $animalDamDamInfo->dob : '' ?></time></a></span>
													<ul>
														<!-- <li><span class="wr nobg inexistant"><a>&nbsp;</a></span></li> -->
														<li class="<?php if(!$animal_dam_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalDamDamSirInfo) && empty($animalDamDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamSirInfo->image) ? base_url()."uploads/animals/".$animalDamDamSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamSirInfo->id)) ? $animalDamDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamSirInfo->name) ? ucfirst($animalDamDamSirInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalDamDamSirInfo->dob) ? $animalDamDamSirInfo->dob : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_dam_dam_sir_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalDamDamSirSirInfo) && empty($animalDamDamSirSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamSirSirInfo->image) ? base_url()."uploads/animals/".$animalDamDamSirSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamSirSirInfo->id)) ? $animalDamDamSirSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamSirSirInfo->name) ?  ucfirst($animalDamDamSirSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamDamSirSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_dam_dam_sir_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalDamDamSirDamInfo) && empty($animalDamDamSirDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamSirDamInfo->image) ? base_url()."uploads/animals/".$animalDamDamSirDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamSirDamInfo->id)) ? $animalDamDamSirDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamSirDamInfo->name) ? ucfirst($animalDamDamSirDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamDamSirDamInfo->dob) ? $animalDamDamSirDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>

														<li class="<?php if(!$animal_dam_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalDamDamDamInfo) && empty($animalDamDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamDamInfo->image) ? base_url()."uploads/animals/".$animalDamDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamDamInfo->id)) ? $animalDamDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamDamInfo->name) ? ucfirst($animalDamDamDamInfo->name) : '' ?><br><time class="dob"><?php echo !empty($animalDamDamDamInfo->dob) ? $animalDamDamDamInfo->dob : '' ?> </time></a></span>
															<ul>
																<li class="<?php if(!$animal_dam_dam_dam_sir_status) { echo 'no-parent'; } else { echo 'male'; } ?>"><span class="wr <?php echo (!empty($animalDamDamDamSirInfo) && empty($animalDamDamDamSirInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamDamSirInfo->image) ? base_url()."uploads/animals/".$animalDamDamDamSirInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamDamSirInfo->id)) ? $animalDamDamDamSirInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamDamSirInfo->name) ?  ucfirst($animalDamDamDamSirInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamDamDamSirInfo->dob) ?> </time></a></span></li>

																<li class="<?php if(!$animal_dam_dam_dam_dam_status) { echo 'no-parent'; } else { echo 'female'; } ?>"><span class="wr <?php echo (!empty($animalDamDamDamDamInfo) && empty($animalDamDamDamDamInfo->image)) ? 'nobg' : '' ?>" style="background-image:url('<?php echo !empty($animalDamDamDamDamInfo->image) ? base_url()."uploads/animals/".$animalDamDamDamDamInfo->image : ''; ?>');"><a href="<?php echo (!empty($animalDamDamDamDamInfo->id)) ? $animalDamDamDamDamInfo->id : 'Javascript:void(0);'; ?>"><?php echo !empty($animalDamDamDamDamInfo->name) ? ucfirst($animalDamDamDamDamInfo->name) : '' ?><br><time class="dob"> <?php echo !empty($animalDamDamDamDamInfo->dob) ? $animalDamDamDamDamInfo->dob : '' ?> </time></a></span></li>
															</ul>
														</li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</div>
										
							</div>
                    	</div>
	                    
	                </div>
					
                </div>
        </div>
    </div>
</div>