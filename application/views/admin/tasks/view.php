<style type="text/css">
    .section {
          display: none;
        }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Task</th>
                            <th>Submitted On</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach ($tasks as $key => $task): ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $task->action ?></td>
                                <td><span class=""><i class="fa fa-clock-o"></i> <?php echo $task->task_date ?></span> </td>
                                
                                <td>
                                     <?php if($task->task_status == 0): ?>
                                        <a href="<?php echo base_url('admin/tasks/approve-claim/'.$task->id.'/'.$task->user_id.'/'.$task->animal_id) ?>"><button type="button" class="btn btn-info btn-circle btn-xs" data-toggle="tooltip" data-original-title="Approve"><i class="fa fa-check"></i></button></a>
                                        <a href="<?php echo base_url('admin/tasks/reject-claim/'.$task->id) ?>"><button type="button" class="btn btn-danger btn-circle btn-xs" data-toggle="tooltip" data-original-title="Reject"><i class="fa fa-times"></i></button></a>
                                    <?php endif; ?>
                                    
                                    <a href="javascript:void(0);" class="btn btn-success view_task"><i class="fa fa-eye"></i> View</a>
                                    <a href="<?php echo $task->link ?>" class="btn btn-info"><i class="fa fa-link"></i> Go To</a>
                                    
                                </td>
                            </tr>
                            <tr class="section">
                                <td colspan="4">
                                    <table class="table">
                                    <?php foreach (unserialize($task->extra_info) as $key => $value) {
                                        ?>
                                        <tr>
                                            <th><?php echo ucfirst($key); ?></th>
                                            <td><?php echo ucfirst($value); ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    </table>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.view_task').click(function() {
      $(this)
        .closest('tr')
        .next('.section')
        .toggle('slow');
    });
</script>