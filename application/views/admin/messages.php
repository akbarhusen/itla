<?php if(!empty($this->session->flashdata('errors'))): ?>
	<div class="form-validation">
	<?php echo $this->session->flashdata('errors'); ?>
	</div>
	<br>
<?php endif; ?>

<?php if(!empty($this->session->flashdata('success'))): ?>
	<div class="alert alert-success">
		<?php echo $this->session->flashdata('success'); ?>
	</div>
<?php endif; ?>

<?php if(!empty($this->session->flashdata('error'))): ?>
	<div class="form-validation">
	<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
	<br>
<?php endif; ?>