<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Members </h3>
            <p class="text-muted m-b-20">All members</p>
            <div class="table-responsive">
                <table class="table" id="memberTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Member</th>
                            <th>Membership Type</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Renewal Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>