 <div class="row">
	    <div class="col-sm-12">
	        <div class="white-box p-l-20 p-r-20">
	            <div class="row">
                    <div class="col-md-12">
                    	<?php $this->load->view('admin/messages'); ?>
                        <form class="form-horizontal" data-toggle="validator" action="<?php echo base_url('user/MyAnimals/save'); ?>" method="post" enctype="multipart/form-data">
                        	<!-- CSRF token -->
        					<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                            <input type="hidden" name="type" value="<?php echo $type ?>" />
                            <div class="form-group col-md-12">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="dropify" name="image" data-height="300" />
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="name"  required="" value="">
                                </div>
                            </div>

                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">TLBAA Number</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" name="tlba_number"  required="" value="">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="form-group col-md-6">
                                <label class="col-md-12">Sex</label>
                                <div class="col-md-12">
                                	<select class="form-control form-control-line" name="sex" required="">
                                		<option value=""></option>
                                		<option value="Bull">Bull</option>
                                		<option value="Cow">Cow</option>
                                        <option value="Steer">Steer</option>
                                	</select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Private Herd #</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="private_herd" value="">
                                </div>
                            </div>

                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Private Herd Location</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" name="private_herd_location" value="">
                                    </div>
                                </div>

                            <div class="form-group col-md-6">
                                <label class="col-md-12">Sire</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="sire" name="sire" value="">
                                </div>
                            </div>
                            <input type="hidden" id="sire_id" name="sire_id" value="0">
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Dam</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="dam" name="dam" value="">
                                </div>
                            </div>
                            <input type="hidden" id="dam_id" name="dam_id" value="0">
                            <?php endif; ?>

                            <div class="form-group col-md-6">
                                <label class="col-md-12">DOB</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line mydatepicker" id="dob" name="dob" value="" autocomplete="off">
                                </div>
                            </div>
                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Service</label>
                                <div class="col-md-12">
                                    <select class="form-control form-control-line" name="service" id="service">
                                        <option value="">Select</option>
                                        <option value="Natutal">Natutal</option>
                                        <option value="Emb Transfer">Emb Transfer</option>
                                        <option value="A.I">A.I</option>
                                        <option value="Clone">Clone</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="col-md-12">MILLENNIUM FUTURITY</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="millennium_futurity" name="millennium_futurity" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BIRTH WEIGHT</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="birth_weight" name="birth_weight" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BRAND LOCATION</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="brand_location" name="brand_location" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">ORIGINAL OWNERSHIP DATE</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line mydatepicker" id="original_ownership_date" name="original_ownership_date" value="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">REGION</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="religion" name="religion" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BREEDER</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="breeder" name="breeder" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Color</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="color" name="color" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">OCV</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="ocv" name="ocv" value="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">TWIN</label>
                                <div class="col-md-12">
                                    <select name="twin" class="form-control form-control-line">
                                        <option value="No">No</option>
                                        <option value="Yes">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            <div class="form-group col-md-6">
                                <label class="col-md-12">DISPOSAL DATE</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line mydatepicker" id="disposal_date" name="disposal_date" value="" autocomplete="off">
                                </div>
                            </div>
                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BRAND</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="brand" name="brand" value="">
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if($type == 'transfer-into-inventory'): ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Seller Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="seller_name" name="seller_name" value="">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Seller Phone</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="seller_phone" name="seller_phone" value="">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($type == 'transfer-out-inventory'): ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Buyer Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="buyer_name" name="buyer_name" value="">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Buyer Phone</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="buyer_phone" name="buyer_phone" value="">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-rounded btn-primary disabled">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>