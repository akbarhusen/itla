 <div class="row">
	    <div class="col-sm-12">
	        <div class="white-box p-l-20 p-r-20">
	            <div class="row">
                    <div class="col-md-12">
                    	<?php $this->load->view('admin/messages'); ?>
                        <form class="form-horizontal" data-toggle="validator" action="<?php echo base_url('user/MyAnimals/update'); ?>" method="post" enctype="multipart/form-data">
                        	<!-- CSRF token -->
        					<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                            <input type="hidden" name="type" value="<?php echo $type?>" />
                            <input type="hidden" name="id" value="<?php echo $animal->id ?>">
                            <div class="form-group col-md-12">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" class="dropify" name="image" data-default-file="<?php echo base_url()."uploads/animals/".$animal->image ?>" data-height="300" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="name"  required="" value="<?php echo $animal->name ?>">
                                </div>
                            </div>

                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">TLBAA Number</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="tlba_number"  required="" value="<?php echo $animal->tlba_number ?>">
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Sex</label>
                                <div class="col-md-12">
                                	<select class="form-control form-control-line" name="sex" required="">
                                		<option value=""></option>
                                		<option <?php echo ($animal->sex == 'Bull') ? 'selected' : '' ?> value="Bull">Bull</option>
                                		<option <?php echo ($animal->sex == 'Cow') ? 'selected' : '' ?> value="Cow">Cow</option>
                                        <option <?php echo ($animal->sex == 'Steer') ? 'selected' : '' ?> value="Steer">Steer</option>
                                	</select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Private Herd #</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="private_herd"  value="<?php echo $animal->private_herd ?>">
                                </div>
                            </div>
                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Private Herd Location</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="private_herd_location"  value="<?php echo $animal->private_herd_location ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Sire</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="sire" name="sire" value="<?php echo $animal->sire_name ?>" placeholder="Leave blank if N/A">
                                </div>
                            </div>
                            <input type="hidden" id="sire_id" name="sire_id" value="<?php echo $animal->sire ?>">
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Dam</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="dam" name="dam" value="<?php echo $animal->dam_name ?>" placeholder="Leave blank if N/A">
                                </div>
                            </div>
                            <input type="hidden" id="dam_id" name="dam_id" value="<?php echo $animal->dam ?>">
                            <?php endif; ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">DOB</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line mydatepicker" id="dob" name="dob" autocomplete="off" value="<?php echo $animal->dob ?>">
                                </div>
                            </div>
                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>  
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Service</label>
                                <div class="col-md-12">
                                    <select class="form-control form-control-line" name="service" id="service">
                                        <option value="">Select</option>
                                        <option <?php echo ($animal->service == 'Natutal') ? 'selected' : '' ?> value="Natutal">Natutal</option>
                                        <option <?php echo ($animal->service == 'Emb Transfer') ? 'selected' : '' ?> value="Emb Transfer">Emb Transfer</option>
                                        <option <?php echo ($animal->service == 'A.I') ? 'selected' : '' ?> value="A.I">A.I</option>
                                        <option <?php echo ($animal->service == 'Clone') ? 'selected' : '' ?> value="Clone">Clone</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">MILLENNIUM FUTURITY</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="millennium_futurity" name="millennium_futurity" value="<?php echo $animal->millennium_futurity ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BIRTH WEIGHT</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="birth_weight" name="birth_weight" value="<?php echo $animal->birth_weight ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BRAND LOCATION</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="brand_location" name="brand_location" value="<?php echo $animal->brand_location ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">ORIGINAL OWNERSHIP DATE</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line mydatepicker" id="original_ownership_date" name="original_ownership_date" value="<?php echo $animal->original_ownership_date ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">REGION</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="religion" name="religion" value="<?php echo $animal->religion ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BREEDER</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="breeder" name="breeder" value="<?php echo $animal->breeder ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">Color</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="color" name="color" value="<?php echo $animal->color ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">OCV</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="ocv" name="ocv" value="<?php echo $animal->ocv ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">TWIN</label>
                                <div class="col-md-12">
                                    <select name="twin" class="form-control form-control-line">
                                        <option <?php echo ($animal->twin=='No') ? 'selected' : ''; ?> value="No">No</option>
                                        <option <?php echo ($animal->twin=='Yes') ? 'selected' : ''; ?> value="Yes">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">DISPOSAL DATE</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line mydatepicker" id="disposal_date" name="disposal_date" value="<?php echo $animal->disposal_date ?>">
                                </div>
                            </div>
                            <?php if($type == 'normal' || $type == 'register-new' || $type == 'dual-register-new'): ?>
                            <div class="form-group col-md-6">
                                <label class="col-md-12">BRAND</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" id="brand" name="brand" value="<?php echo $animal->brand ?>">
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if($type == 'transfer-into-inventory'): ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Seller Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="seller_name" name="seller_name" value="<?php echo $animal->seller_name ?>">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Seller Phone</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="seller_phone" name="seller_phone" value="<?php echo $animal->seller_phone ?>">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($type == 'transfer-out-inventory'): ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Buyer Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="buyer_name" name="buyer_name" value="<?php echo $animal->buyer_name ?>">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Buyer Phone</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control form-control-line" id="buyer_phone" name="buyer_phone" value="<?php echo $animal->buyer_phone ?>">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-rounded btn-primary disabled">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>