<div class="row">
    <div class="col-lg-12">
    	<h1>What do you want to do?</h1>
        <div class="row">

            <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <a href="<?php echo base_url('/user/my-animals/create/transfer-into-inventory') ?>" class="btn btn-custom m-b-10 ">Transfer Into My Inventory</a>
                            <p>Select <b>Transfer Into My Inventory</b> if you've bought an animal (ITLA, TLBA or Cattleman's) and need to transfer it into your inventory.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <a href="<?php echo base_url('/user/my-animals/create/transfer-out-inventory') ?>" class="btn btn-custom m-b-10">Transfer Out Of My Inventory</a>
                            <p>Select <b>Transfer Out Of My Inventory</b> if you've sold an animal and need to transfer it to new owner. You'll need to pick the animal in My Animals and then Click on Transfer Animal.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <a href="<?php echo base_url('/user/my-animals/create/register-new') ?>" class="btn btn-custom m-b-10">Register A New Animal</a>
                            <p>Select <b>Register A New Animal</b> if you want to register a new animal that has not been registered before with ITLA, TLBAA or Cattleman's.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <a href="<?php echo base_url('/user/my-animals/create/dual-register-new') ?>" class="btn btn-custom m-b-10">Dual Register A New Animal</a>
                            <p>Select <b>Dual Register A New Animal</b> if you want to register with ITLA that is registered in your name with TLBAA or Cattleman's.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>