<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ShoppingCart extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
    }
 
    public function index(){
        $data = array();
        $data['page_title'] = 'Shopping Cart';
        $data['count'] = 15;
        $data['main_content'] = $this->load->view('user/ShoppingCart/view', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

}