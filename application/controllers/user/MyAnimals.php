<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyAnimals extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
        $this->load->model('Animal_model');
    }
 
    public function index(){
        $data = array();
        $data['page_title'] = 'My Animals';
        $data['myAnimals'] = $this->Animal_model->myAnimals();
        $data['main_content'] = $this->load->view('user/MyAnimals/view', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function create($type=''){
        $data = array();
        $data['page_title'] = 'Create - My Animals';
        $data['type'] = $type;
        $data['main_content'] = $this->load->view('user/MyAnimals/create', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function save() {
        $imageName = '';
        if(!empty($_FILES['image']['name'])) {
            $resource = $this->common_model->upload_image();
            $imageName = $resource['orig_name'];
        }
        if ($_POST) {
            $data = array(
                'name' => $_POST['name'],
                'tlba_number' => !empty($_POST['tlba_number']) ? $_POST['tlba_number'] : '',
                'sex' => $_POST['sex'],
                'private_herd' => $_POST['private_herd'],
                'private_herd_location' => !empty($_POST['private_herd_location']) ? $_POST['private_herd_location'] : '',
                'sire' => !empty($_POST['sire_id']) ? $_POST['sire_id'] : '',
                'dam' => !empty($_POST['dam_id']) ? $_POST['dam_id'] : '',
                'sire_name' => !empty($_POST['sire']) ? $_POST['sire'] : '',
                'dam_name' => !empty($_POST['dam']) ? $_POST['dam'] : '',
                'user_id' => ($this->session->userdata('role') == 'user') ? $this->session->userdata('id'): 0,
                'dob' => $_POST['dob'],
                'service' => !empty($_POST['service']) ? $_POST['service'] : '',
                'millennium_futurity' => !empty($_POST['millennium_futurity']) ? $_POST['millennium_futurity'] : '',
                'birth_weight' => !empty($_POST['birth_weight']) ? $_POST['birth_weight'] : '',
                'brand_location' => !empty($_POST['brand_location']) ? $_POST['brand_location'] : '',
                'original_ownership_date' => !empty($_POST['original_ownership_date']) ? $_POST['original_ownership_date'] : '',
                'religion' => !empty($_POST['religion']) ? $_POST['religion'] : '',
                'breeder' => !empty($_POST['breeder']) ? $_POST['breeder'] : '',
                'color' => !empty($_POST['color']) ? $_POST['color'] : '',
                'ocv' => !empty($_POST['ocv']) ? $_POST['ocv'] : '',
                'twin' => !empty($_POST['twin']) ? $_POST['twin'] : '',
                'disposal_date' => $_POST['disposal_date'],
                'brand' => !empty($_POST['brand']) ? $_POST['brand'] : '',
                'seller_name' => !empty($_POST['seller_name']) ? $_POST['seller_name'] : '',
                'seller_phone' => !empty($_POST['seller_phone']) ? $_POST['seller_phone'] : '',
                'buyer_name' => !empty($_POST['buyer_name']) ? $_POST['buyer_name'] : '',
                'buyer_phone' => !empty($_POST['buyer_phone']) ? $_POST['buyer_phone'] : '',
                'type' => !empty($_POST['type']) ? $_POST['type'] : '',
                'image' => $imageName,
                'status' => ($this->session->userdata('role') == 'admin') ? 1 : 0,
                'created_at' => current_datetime()
            );

            $data = $this->security->xss_clean($data);
            $animal_id = $this->common_model->insert($data, 'animals');
            if($this->session->userdata('role') != 'admin') {
                $taskdata = array(
                            'user_id' => $this->session->userdata('id'),
                            'animal_id' => $animal_id,
                            'action' => 'New animal added.',
                            'link' => base_url('/admin/animal/'.$animal_id),
                            'status' => 1,
                            'extra_info' => serialize(array(
                                    'ITLA' => $animal_id,
                                    'name' => $_POST['name'],
                                    'private_herd' => $_POST['private_herd'],
                                    'dob' => $_POST['dob'],
                                    'type' => !empty($_POST['type']) ? $_POST['type'] : '',
                                )
                                ),
                            'created_at' => date('Y-m-d H:i:s')
                        );
                $taskdata = $this->security->xss_clean($taskdata);
                $this->common_model->insert($taskdata, 'claim_animals');
            }

            $this->session->set_flashdata('message', 'Animal added Successfully');
            if($this->session->userdata('role') == 'user')
                redirect(base_url('user/my-animals'));
            else
                redirect(base_url('admin/animals'));
        }
    }

    public function getAnimalType($type) {
        echo $this->Animal_model->getAnimalType($type);
    }

    public function edit($id) {
        $data = array();
        $data['page_title'] = 'Edit - My Animals';
        $data['animal'] = $this->Animal_model->getAnimalInfo($id);
        $data['type'] = $data['animal']->type;
        $data['main_content'] = $this->load->view('user/MyAnimals/edit', $data, TRUE);
        $this->load->view('admin/index', $data);   
    }

    public function update() {
        
        if(!empty($_FILES['image']['name'])) {
            $resource = $this->common_model->upload_image();
            if($resource == 'failed') {
                $this->session->set_flashdata('message', 'Error during uploading image. Please try again with image file.');
                if($this->session->userdata('role') == 'user')
                    redirect(base_url('user/my-animals'));
                else
                    redirect(base_url('admin/animals'));
            }
            $imageName = $resource['orig_name'];
            $getSingle = $this->common_model->getOne($_POST['id'], 'animals');
            unlink('./uploads/animals/'.$getSingle->image);
        }
        if ($_POST) {
            $data = array(
                'name' => $_POST['name'],
                'tlba_number' => !empty($_POST['tlba_number']) ? $_POST['tlba_number'] : '',
                'sex' => $_POST['sex'],
                'private_herd' => $_POST['private_herd'],
                'private_herd_location' => !empty($_POST['private_herd_location']) ? $_POST['private_herd_location'] : '',
                'sire' => !empty($_POST['sire_id']) ? $_POST['sire_id'] : '',
                'dam' => !empty($_POST['dam_id']) ? $_POST['dam_id'] : '',
                'sire_name' => !empty($_POST['sire']) ? $_POST['sire'] : '',
                'dam_name' => !empty($_POST['dam']) ? $_POST['dam'] : '',
                'user_id' => ($this->session->userdata('role') == 'user') ? $this->session->userdata('id'): 0,
                'dob' => $_POST['dob'],
                'service' => !empty($_POST['service']) ? $_POST['service'] : '',
                'millennium_futurity' => !empty($_POST['millennium_futurity']) ? $_POST['millennium_futurity'] : '',
                'birth_weight' => !empty($_POST['birth_weight']) ? $_POST['birth_weight'] : '',
                'brand_location' => !empty($_POST['brand_location']) ? $_POST['brand_location'] : '',
                'original_ownership_date' => !empty($_POST['original_ownership_date']) ? $_POST['original_ownership_date'] : '',
                'religion' => !empty($_POST['religion']) ? $_POST['religion'] : '',
                'breeder' => !empty($_POST['breeder']) ? $_POST['breeder'] : '',
                'color' => !empty($_POST['color']) ? $_POST['color'] : '',
                'ocv' => !empty($_POST['ocv']) ? $_POST['ocv'] : '',
                'twin' => !empty($_POST['twin']) ? $_POST['twin'] : '',
                'disposal_date' => $_POST['disposal_date'],
                'brand' => !empty($_POST['brand']) ? $_POST['brand'] : '',
                'seller_name' => !empty($_POST['seller_name']) ? $_POST['seller_name'] : '',
                'seller_phone' => !empty($_POST['seller_phone']) ? $_POST['seller_phone'] : '',
                'buyer_name' => !empty($_POST['buyer_name']) ? $_POST['buyer_name'] : '',
                'buyer_phone' => !empty($_POST['buyer_phone']) ? $_POST['buyer_phone'] : ''
            );
            if(!empty($_FILES['image']['name'])) {
                $data['image'] = $imageName;
            }
            $data = $this->security->xss_clean($data);

            $this->common_model->edit_option($data, $_POST['id'], 'animals');
            $this->session->set_flashdata('message', 'Animal Updated Successfully');
            if($this->session->userdata('role') == 'user')
                redirect(base_url('user/my-animals'));
            else
                redirect(base_url('admin/animals'));

        } 
    }

    public function delete($id) {
        $this->common_model->delete($id, 'animals');
        $this->session->set_flashdata('message', 'Animal deleted Successfully');
            if($this->session->userdata('role') == 'user')
                redirect(base_url('user/my-animals'));
            else
                redirect(base_url('admin/animals'));
    }

    public function claim($id) {
        if($this->Animal_model->claim($id)) {
            $this->session->set_flashdata('message', 'Animal claimed Successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Animal already claimed');
        }
        redirect(base_url('admin/animals'));
    }

    public function registerAnimal() {
        $data = array();
        $data['page_title'] = 'Register / Tranfer';
        $data['main_content'] = $this->load->view('user/MyAnimals/register-animal-type', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function updateStatus($status, $id) {

        if($status == 'approve')
            $data = array('status' => 1, 'type' => 'normal');
        else
            $data = array('status' => 2);
        
        $data = $this->security->xss_clean($data);
        $this->common_model->edit_option($data, $id, 'animals');
        $this->session->set_flashdata('message', 'Animal '.$status.' Successfully');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function exportCsv() {
        $header = array();
        $data = $this->Animal_model->myAnimals();
        foreach ($data as $key => $animal) {
            foreach ($animal as $key1 => $value) {
                $header[] = $key1;
            }
            break;
        }
        $filename = "my_animals.csv";
        $fp = fopen('php://output', 'w');
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $header);

        foreach ($data as $key => $animal) {
            $myArray = json_decode(json_encode($animal), true);
            fputcsv($fp, $myArray);
        }
        exit;
    }

    public function getMyAnimals() {
        // POST data
        $postData = $this->input->post();

        // Get data
        $data = $this->Animal_model->getMyAnimalsAjx($postData);

        echo json_encode($data);
    }

    
}