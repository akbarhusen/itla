<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MembershipDirectory extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
        $this->load->model('Animal_model');
    }
 
    public function index(){
        $data = array();
        $data['page_title'] = 'Membership Directory';
        // $data['members'] = $this->Animal_model->members();
        $data['main_content'] = $this->load->view('user/MembershipDirectory/view', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

}