<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
set_time_limit(0);


class Members extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
        $this->load->model('Animal_model');
        $this->load->model('Member_model');
    }

    public function index(){
        $data = array();
        $data['page_title'] = 'Members';
        // $data['members'] = $this->Animal_model->members();
        $data['main_content'] = $this->load->view('admin/members/view', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function add(){
        $data = array();
        $data['page_title'] = 'Add Member';
        $data['main_content'] = $this->load->view('admin/members/add', $data, TRUE);
        $this->load->view('admin/index', $data);
    }
    /*import animal */
    public function import_animal(){
        $data = array();
        $data['page_title'] = 'Import Animal';
        $data['main_content'] = $this->load->view('admin/members/import_animal', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function import_animal_csv() {
/*
        if(isset($_FILES["import_animal"]["name"]))
        {
            if($_FILES["import_animal"]["name"] != "")
            {
              if($_FILES["import_animal"]["size"] > 0)
              { */  
                $file="";  
                $file=fopen($_FILES["import_animal"]["tmp_name"], "r");
                $row = fgetcsv($file,1000,",");
                while (($column = fgetcsv($file,1000,",")) !== FALSE) 
                {

                   $name = $column[0];

                   $created_at = $column[1];
                   $changed_createdat = strtotime($created_at);
                   $new_date_created = date("Y-m-d H:i:s", $changed_createdat);    

                   $itla = $column[2];
                
                   $tbla = $column[3];
                   $sex = $column[4];
                   $sire = $column[5];
                   
                   $dam = $column[6];
                   $color = $column[7];
                   $service = $column[8];
                   $ocv = $column[9];
                   $private_heard = $column[10];
                   $private_heard_location = $column[11];

                   $dob = $column[12];
                   $changed_dob = strtotime($dob);
                   $new_date_dob = date("Y-m-d H:i:s", $changed_dob);

                   $birth_weight = $column[13];
                   $status = $column[14];
                   $disposal_date = $column[15];
                   $brand_location = $column[16];
                   $owner = $column[17];
                   $orignal_ownership_date = $column[18];
                   $changed_ownership_date = strtotime($orignal_ownership_date);
                   $new_date_ownership = date("Y-m-d H:i:s", $changed_ownership_date);

                   $breeder = !empty($column[19]) ? $column[19] :'';


                   $select_animal = $this->common_model->select('animals');
                   $this->db->select();
                   $this->db->from('animals');
                   $this->db->where('name',$name);
                   $this->db->where('dob',$new_date_dob);
                   $query = $this->db->get();
                   $result = $query->row_array();

                   $result_data = $this->common_model->select('animals');
                   $this->db->from('animals');
                   $this->db->where('name',$sire);
                   //$this->db->where('dam_name',$sire);
                   $query1 = $this->db->get();
                   $result_data1 = $query1->row_array();

                    //print_r($result_data1['id']);exit();

                   /*get sire id*/
                   $data = array(
                      'name'  => $name,
                      'created_at'=>$new_date_created,
                      'itla' => $itla,
                      'tlba_number' => $tbla,
                      'sex' =>$sex,
                      //'sire' =>$result_data1['id'],
                     //'dam' =>$result_data1['id'],
                      'sire_name' =>$sire,
                      'dam_name' =>$dam,
                      'color' =>$color,
                      'service' =>$service,
                      'ocv' =>$ocv,
                      'private_herd' =>$private_heard,
                      'private_herd_location' =>$private_heard_location,
                      'dob' =>$new_date_dob,
                      'birth_weight' =>$birth_weight,
                      'status' =>$status,
                      'disposal_date' =>$disposal_date,
                      'brand_location' =>$brand_location,
                      'owner' =>$owner,
                      'original_ownership_date' =>$new_date_ownership,
                      'breeder' =>$breeder,
                  );
                   //dd($query->num_rows());
                
               if($query->num_rows() == 0)
               {
                $this->common_model->insert($data, 'animals');

            }
            else
            {
                $this->common_model->edit_option($data,$result['id'],'animals');

            }
           }
        //$data = $this->security->xss_clean($data);
    $this->session->set_flashdata('message', 'Please select valid csv file');
    redirect(base_url('admin/members/import_animal'));
    }   
               /*}
                   $this->session->set_flashdata('message', 'Import animal Successfully');
                    redirect(base_url('admin/members/import_animal'));

        }
            $this->session->set_flashdata('message', 'Size Not Valid');
            redirect(base_url('admin/members/import_animal'));
    }
            */

   



/* End import animal */

public function save() {
    $imageName = '';
    if(!empty($_FILES['image']['name'])) {
        $resource = $this->common_model->upload_image();
        $imageName = $resource['orig_name'];
    }
    $data = array(
        'member' => $_POST['member'],
        'membership_type' => $_POST['membership_type'],
        'address' => $this->input->post('address'),
        'email' => $_POST['email'],
        'ranch' => $_POST['ranch'],
        'partnership' => !empty($_POST['partnership']) ? 1 : 0,
        'address_type' => $_POST['address_type'],
        'address' => $_POST['address'],
        'region' => $_POST['region'],
        'mailing_address_type' => !empty($_POST['mailing_address_type']) ? 1 : 0,
        'website' => $_POST['website'],
        'phone' => $_POST['phone'],
        'date_joined' => $_POST['date_joined'],
        'expiration_date' => $_POST['expiration_date'],
        'birth_date' => $_POST['birth_date'],
        'opt_in' => !empty($_POST['opt_in']) ? 1 : 0,
        'renewal_date' => $_POST['renewal_date'],
        'brand' => $imageName,
    );

    $data = $this->security->xss_clean($data);
    $this->common_model->insert($data, 'members');
    $this->session->set_flashdata('message', 'Member added Successfully');
    redirect(base_url('admin/members'));
}

public function getMembers() {
        // POST data
    $postData = $this->input->post();

        // Get data
    $data = $this->Member_model->getMembersAjx($postData);

    echo json_encode($data);
}

public function edit($id) {
    $data = array();
    $data['page_title'] = 'Edit Member';
    $data['member'] = $this->common_model->getOne($id, 'members');
    $data['main_content'] = $this->load->view('admin/members/edit', $data, TRUE);
    $this->load->view('admin/index', $data);
}

public function delete($id) {
    $this->common_model->delete($id, 'members');
    $this->session->set_flashdata('message', 'Member deleted Successfully');
    redirect($_SERVER['HTTP_REFERER']);
}

public function update() {
    $imageName = '';
    if(!empty($_FILES['image']['name'])) {
        $resource = $this->common_model->upload_image();
        $imageName = $resource['orig_name'];
        $getSingle = $this->common_model->getOne($this->input->post('id'), 'members');

        if(!empty($getSingle->brand) && file_exists('./uploads/animals/'.$getSingle->brand)) {
            unlink('./uploads/animals/'.$getSingle->brand);
        }
    }
    $data = array(
        'member' => $_POST['member'],
        'membership_type' => $_POST['membership_type'],
        'address' => $this->input->post('address'),
        'email' => $_POST['email'],
        'ranch' => $_POST['ranch'],
        'partnership' => !empty($_POST['partnership']) ? 1 : 0,
        'address_type' => $_POST['address_type'],
        'address' => $_POST['address'],
        'region' => $_POST['region'],
        'mailing_address_type' => !empty($_POST['mailing_address_type']) ? 1 : 0,
        'website' => $_POST['website'],
        'phone' => $_POST['phone'],
        'date_joined' => $_POST['date_joined'],
        'expiration_date' => $_POST['expiration_date'],
        'birth_date' => $_POST['birth_date'],
        'opt_in' => !empty($_POST['opt_in']) ? 1 : 0,
        'renewal_date' => $_POST['renewal_date'],
    );
    if(!empty($_FILES['image']['name'])) {
        $data['brand'] = $imageName;
    }
    $data = $this->security->xss_clean($data);
    $this->common_model->edit_option($data, $this->input->post('id'), 'members');
    $this->session->set_flashdata('message', 'Member updated Successfully');
    redirect($_SERVER['HTTP_REFERER']);
}

public function getMembersAutoComplete() {
    echo $this->Member_model->getMembersAutoComplete();
}

public function single($id) {
    $data = array();
    $data['member'] = $this->common_model->getOne($id, 'members');
    $data['page_title'] = ucfirst($data['member']->member);
    $data['main_content'] = $this->load->view('admin/members/single', $data, TRUE);
    $this->load->view('admin/index', $data);
}
}