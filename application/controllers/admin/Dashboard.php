<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// *************************************************************************
// *                                                                       *
// * Optimum LinkupComputers                              *
// * Copyright (c) Optimum LinkupComputers. All Rights Reserved                     *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: info@optimumlinkupsoftware.com                                 *
// * Website: https://optimumlinkup.com.ng								   *
// * 		  https://optimumlinkupsoftware.com							   *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.                              *
// *                                                                       *
// *************************************************************************

//LOCATION : application - controller - Dashboard.php

class Dashboard extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
        $this->load->model('Animal_model');
    }

    /****************Function login**********************************
     * @type            : Function
     * @function name   : index
     * @description     : This redirect to dashboard automatically 
     *                    
     *                       
     * @param           : null 
     * @return          : null 
     * ********************************************************** */
	 
    public function index(){
        $data = array();
        $data['page_title'] = 'Animals';
        $data['animals'] = $this->Animal_model->animals();
        $data['main_content'] = $this->load->view('admin/animals/view', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function getAnimals() {
        // POST data
        $postData = $this->input->post();

        // Get data
        $data = $this->Animal_model->getAnimalsAjx($postData);

        echo json_encode($data);
    }


/****************Function login**********************************
     * @type            : Function
     * @function name   : backup
     * @description     : Force database to be downloaded. 
     *                    if user or admin click on download button.
     *                       
     * @param           : null 
     * @return          : null 
     * ********************************************************** */
	 
    public function backup($fileName='db_backup.zip'){
        $this->load->dbutil();
        $backup =& $this->dbutil->backup();
        $this->load->helper('file');
        write_file(FCPATH.'/downloads/'.$fileName, $backup);
        $this->load->helper('download');
        force_download($fileName, $backup);
    }

    public function any($id) {
        $data = array();
        $data['page_title'] = 'Animals';
        $data['animal'] = $this->common_model->getOne($id, 'animals');
        $data['animalSirInfo'] = $this->common_model->getOne((!empty($data['animal']->sire) ? $data['animal']->sire : '0'), 'animals');
        $data['animalDamInfo'] = $this->common_model->getOne((!empty($data['animal']->dam) ? $data['animal']->dam : '0'), 'animals');
        $data['animal_sir_status'] = false;
        $data['animal_dam_status'] = false;
        if(!empty($data['animalSirInfo']))
            $data['animal_sir_status'] = true;
        if(!empty($data['animalDamInfo']))
            $data['animal_dam_status'] = true;

        $data['animalSirSirInfo'] = $this->common_model->getOne((!empty($data['animalSirInfo']->sire) ? $data['animalSirInfo']->sire : '0'), 'animals');
        $data['animalSirDamInfo'] = $this->common_model->getOne((!empty($data['animalSirInfo']->dam) ? $data['animalSirInfo']->dam : '0'), 'animals');
        $data['animal_sir_sir_status'] = false;
        $data['animal_sir_dam_status'] = false;
        if(!empty($data['animalSirSirInfo']))
            $data['animal_sir_sir_status'] = true;
        if(!empty($data['animalSirDamInfo']))
            $data['animal_sir_dam_status'] = true;

        $data['animalDamSirInfo'] = $this->common_model->getOne((!empty($data['animalDamInfo']->sire) ? $data['animalDamInfo']->sire : '0'), 'animals');
        $data['animalDamDamInfo'] = $this->common_model->getOne((!empty($data['animalDamInfo']->dam) ? $data['animalDamInfo']->dam : '0'), 'animals');
        $data['animal_dam_sir_status'] = false;
        $data['animal_dam_dam_status'] = false;
        if(!empty($data['animalDamSirInfo']))
            $data['animal_dam_sir_status'] = true;
        if(!empty($data['animalDamDamInfo']))
            $data['animal_dam_dam_status'] = true;

        $data['animalSirSirSirInfo'] = $this->common_model->getOne((!empty($data['animalSirSirInfo']->sire) ? $data['animalSirSirInfo']->sire : '0'), 'animals');
        $data['animalSirSirDamInfo'] = $this->common_model->getOne((!empty($data['animalSirSirInfo']->dam) ? $data['animalSirSirInfo']->dam : '0'), 'animals');
        $data['animal_sir_sir_sir_status'] = false;
        $data['animal_sir_sir_dam_status'] = false;
        if(!empty($data['animalSirSirSirInfo']))
            $data['animal_sir_sir_sir_status'] = true;
        if(!empty($data['animalSirSirDamInfo']))
            $data['animal_sir_sir_dam_status'] = true;

        $data['animalSirDamSirInfo'] = $this->common_model->getOne((!empty($data['animalSirDamInfo']->sire) ? $data['animalSirDamInfo']->sire : '0'), 'animals');
        $data['animalSirDamDamInfo'] = $this->common_model->getOne((!empty($data['animalSirDamInfo']->dam) ? $data['animalSirDamInfo']->dam : '0'), 'animals');
        $data['animal_sir_dam_sir_status'] = false;
        $data['animal_sir_dam_dam_status'] = false;
        if(!empty($data['animalSirDamSirInfo']))
            $data['animal_sir_dam_sir_status'] = true;
        if(!empty($data['animalSirDamDamInfo']))
            $data['animal_sir_dam_dam_status'] = true;

        $data['animalDamSirSirInfo'] = $this->common_model->getOne((!empty($data['animalDamSirInfo']->sire) ? $data['animalDamSirInfo']->sire : '0'), 'animals');
        $data['animalDamSirDamInfo'] = $this->common_model->getOne((!empty($data['animalDamSirInfo']->dam) ? $data['animalDamSirInfo']->dam : '0'), 'animals');
        $data['animal_dam_sir_sir_status'] = false;
        $data['animal_dam_sir_dam_status'] = false;
        if(!empty($data['animalDamSirSirInfo']))
            $data['animal_dam_sir_sir_status'] = true;
        if(!empty($data['animalDamSirDamInfo']))
            $data['animal_dam_sir_dam_status'] = true;

        $data['animalDamDamSirInfo'] = $this->common_model->getOne((!empty($data['animalDamDamInfo']->sire) ? $data['animalDamDamInfo']->sire : '0'), 'animals');
        $data['animalDamDamDamInfo'] = $this->common_model->getOne((!empty($data['animalDamDamInfo']->dam) ? $data['animalDamDamInfo']->dam : '0'), 'animals');
        $data['animal_dam_dam_sir_status'] = false;
        $data['animal_dam_dam_dam_status'] = false;
        if(!empty($data['animalDamDamSirInfo']))
            $data['animal_dam_dam_sir_status'] = true;
        if(!empty($data['animalDamDamDamInfo']))
            $data['animal_dam_dam_dam_status'] = true;


        //forth leval One
        $data['animalSirSirSirSirInfo'] = $this->common_model->getOne((!empty($data['animalSirSirSirInfo']->sire) ? $data['animalSirSirSirInfo']->sire : '0'), 'animals');
        $data['animalSirSirSirDamInfo'] = $this->common_model->getOne((!empty($data['animalSirSirSirInfo']->dam) ? $data['animalSirSirSirInfo']->dam : '0'), 'animals');
        $data['animal_sir_sir_sir_sir_status'] = false;
        $data['animal_sir_sir_sir_dam_status'] = false;
        if(!empty($data['animalSirSirSirSirInfo']))
            $data['animal_sir_sir_sir_sir_status'] = true;
        if(!empty($data['animalSirSirSirDamInfo']))
            $data['animal_sir_sir_sir_dam_status'] = true;

        //forth leval Two
        $data['animalSirSirDamSirInfo'] = $this->common_model->getOne((!empty($data['animalSirSirDamInfo']->sire) ? $data['animalSirSirDamInfo']->sire : '0'), 'animals');
        $data['animalSirSirDamDamInfo'] = $this->common_model->getOne((!empty($data['animalSirSirDamInfo']->dam) ? $data['animalSirSirDamInfo']->dam : '0'), 'animals');
        $data['animal_sir_sir_dam_sir_status'] = false;
        $data['animal_sir_sir_dam_dam_status'] = false;
        if(!empty($data['animalSirSirDamSirInfo']))
            $data['animal_sir_sir_dam_sir_status'] = true;
        if(!empty($data['animalSirSirDamDamInfo']))
            $data['animal_sir_sir_dam_dam_status'] = true;

        //forth leval Three
        $data['animalSirDamSirSirInfo'] = $this->common_model->getOne((!empty($data['animalSirDamSirInfo']->sire) ? $data['animalSirDamSirInfo']->sire : '0'), 'animals');
        $data['animalSirDamSirDamInfo'] = $this->common_model->getOne((!empty($data['animalSirDamSirInfo']->dam) ? $data['animalSirDamSirInfo']->dam : '0'), 'animals');
        $data['animal_sir_dam_sir_sir_status'] = false;
        $data['animal_sir_dam_sir_dam_status'] = false;
        if(!empty($data['animalSirDamSirSirInfo']))
            $data['animal_sir_dam_sir_sir_status'] = true;
        if(!empty($data['animalSirDamSirDamInfo']))
            $data['animal_sir_dam_sir_dam_status'] = true;

        //forth leval Four
        $data['animalSirDamDamSirInfo'] = $this->common_model->getOne((!empty($data['animalSirDamDamInfo']->sire) ? $data['animalSirDamDamInfo']->sire : '0'), 'animals');
        $data['animalSirDamDamDamInfo'] = $this->common_model->getOne((!empty($data['animalSirDamDamInfo']->dam) ? $data['animalSirDamDamInfo']->dam : '0'), 'animals');
        $data['animal_sir_dam_dam_sir_status'] = false;
        $data['animal_sir_dam_dam_dam_status'] = false;
        if(!empty($data['animalSirDamDamSirInfo']))
            $data['animal_sir_dam_dam_sir_status'] = true;
        if(!empty($data['animalSirDamDamDamInfo']))
            $data['animal_sir_dam_dam_dam_status'] = true;

        //forth leval Five
        $data['animalDamSirSirSirInfo'] = $this->common_model->getOne((!empty($data['animalDamSirSirInfo']->sire) ? $data['animalDamSirSirInfo']->sire : '0'), 'animals');
        $data['animalDamSirSirDamInfo'] = $this->common_model->getOne((!empty($data['animalDamSirSirInfo']->dam) ? $data['animalDamSirSirInfo']->dam : '0'), 'animals');
        $data['animal_dam_sir_sir_sir_status'] = false;
        $data['animal_dam_sir_sir_dam_status'] = false;
        if(!empty($data['animalDamSirSirSirInfo']))
            $data['animal_dam_sir_sir_sir_status'] = true;
        if(!empty($data['animalDamSirSirDamInfo']))
            $data['animal_dam_sir_sir_dam_status'] = true;

        //forth leval Six
        $data['animalDamSirDamSirInfo'] = $this->common_model->getOne((!empty($data['animalDamSirDamInfo']->sire) ? $data['animalDamSirDamInfo']->sire : '0'), 'animals');
        $data['animalDamSirDamDamInfo'] = $this->common_model->getOne((!empty($data['animalDamSirDamInfo']->dam) ? $data['animalDamSirDamInfo']->dam : '0'), 'animals');
        $data['animal_dam_sir_dam_sir_status'] = false;
        $data['animal_dam_sir_dam_dam_status'] = false;
        if(!empty($data['animalDamSirDamSirInfo']))
            $data['animal_dam_sir_dam_sir_status'] = true;
        if(!empty($data['animalDamSirDamDamInfo']))
            $data['animal_dam_sir_dam_dam_status'] = true;

        //forth leval Seven
        $data['animalDamDamSirSirInfo'] = $this->common_model->getOne((!empty($data['animalDamDamSirInfo']->sire) ? $data['animalDamDamSirInfo']->sire : '0'), 'animals');
        $data['animalDamDamSirDamInfo'] = $this->common_model->getOne((!empty($data['animalDamDamSirInfo']->dam) ? $data['animalDamDamSirInfo']->dam : '0'), 'animals');
        $data['animal_dam_dam_sir_sir_status'] = false;
        $data['animal_dam_dam_sir_dam_status'] = false;
        if(!empty($data['animalDamDamSirSirInfo']))
            $data['animal_dam_dam_sir_sir_status'] = true;
        if(!empty($data['animalDamDamSirDamInfo']))
            $data['animal_dam_dam_sir_dam_status'] = true;

        //forth leval Eight
        $data['animalDamDamDamSirInfo'] = $this->common_model->getOne((!empty($data['animalDamDamDamInfo']->sire) ? $data['animalDamDamDamInfo']->sire : '0'), 'animals');
        $data['animalDamDamDamDamInfo'] = $this->common_model->getOne((!empty($data['animalDamDamDamInfo']->dam) ? $data['animalDamDamDamInfo']->dam : '0'), 'animals');
        $data['animal_dam_dam_dam_sir_status'] = false;
        $data['animal_dam_dam_dam_dam_status'] = false;
        if(!empty($data['animalDamDamDamSirInfo']))
            $data['animal_dam_dam_dam_sir_status'] = true;
        if(!empty($data['animalDamDamDamDamInfo']))
            $data['animal_dam_dam_dam_dam_status'] = true;


        // dd($animalDamInfo);
        if(!empty($data['animal']->image)): 
            $data['image'] = base_url()."uploads/animals/".$data['animal']->image;
        else:
            $data['image'] = base_url()."optimum/images/default-image.png";
        endif; 
        $data['main_content'] = $this->load->view('admin/animals/single', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function exportCsv() {
        $header = array();
        $data = $this->Animal_model->exportAnimals();
        foreach ($data as $key => $animal) {
            foreach ($animal as $key1 => $value) {
                $header[] = $key1;
            }
            break;
        }
        $filename = "my_animals.csv";
        $fp = fopen('php://output', 'w');
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        fputcsv($fp, $header);

        foreach ($data as $key => $animal) {
            $myArray = json_decode(json_encode($animal), true);
            fputcsv($fp, $myArray);
        }
        exit;
    }

}