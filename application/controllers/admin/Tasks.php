<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tasks extends CI_Controller {

	public function __construct(){
        parent::__construct();
        check_login_user();
        $this->load->model('common_model');
        $this->load->model('Animal_model');
    }
 
    public function index(){
        $data = array();
        $data['page_title'] = 'Tasks';
        $data['tasks'] = $this->Animal_model->tasks();
        $data['main_content'] = $this->load->view('admin/tasks/view', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    public function approveClaim($id, $userID, $animalID) {
        if ($id) {
            $data = array(
                'status' => 1
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $id, 'claim_animals');

            $data = array(
                'user_id' => $userID
            );
            $data = $this->security->xss_clean($data);
            $this->common_model->edit_option($data, $animalID, 'animals');

            $this->session->set_flashdata('message', 'Claim Request Approved');
            redirect(base_url('admin/tasks'));
        } 
    }

    public function rejectClaim($id) {
        if ($id) {
            
            $this->common_model->delete($id, 'claim_animals');

            $this->session->set_flashdata('message', 'Claim Request deleted');
            redirect(base_url('admin/tasks'));
        } 
    }

}