<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// *************************************************************************
// *                                                                       *
// * Optimum LinkupComputers                              *
// * Copyright (c) Optimum LinkupComputers. All Rights Reserved                     *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * Email: info@optimumlinkupsoftware.com                                 *
// * Website: https://optimumlinkup.com.ng								   *
// * 		  https://optimumlinkupsoftware.com							   *
// *                                                                       *
// *************************************************************************
// *                                                                       *
// * This software is furnished under a license and may be used and copied *
// * only  in  accordance  with  the  terms  of such  license and with the *
// * inclusion of the above copyright notice.                              *
// *                                                                       *
// *************************************************************************

//LOCATION : application - controller - Auth.php

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('login_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('user_agent');
    }


    public function index(){
        $data = array();
        $data['page'] = 'Auth';
        $this->load->view('admin/login', $data);
    }



 /****************Function login**********************************
     * @type            : Function
     * @function name   : log
     * @description     : Authenticatte when uset try lo login. 
     *                    if autheticated redirected to logged in user dashboard.
     *                    Also set some session date for logged in user.   
     * @param           : null 
     * @return          : null 
     * ********************************************************** */
    public function log(){

        if($_POST){ 
            $query = $this->login_model->validate_user();
            //-- if valid
            if($query){
                if($query[0]->locked == 1) {
                    $this->session->set_flashdata('error', 'Your account has been locked contact us to restore your user privilege!');
                    redirect(base_url() . 'auth', 'refresh');
                }
                $data = array();
                foreach($query as $row){
                    $data = array(
                        'id' => $row->id,
                        'name' => $row->first_name,
                        'email' =>$row->email,
                        'role' =>$row->role,
                        'is_login' => TRUE
                    );
                    $this->session->set_userdata($data);
                    $url = base_url('admin/animals');
                }
				redirect(base_url() . 'admin/animals', 'refresh');
            }else{
                $this->session->set_flashdata('error', 'Login failed!');
               redirect(base_url() . 'auth', 'refresh');
            }
            
        }else{
            $this->load->view('auth',$data);
        }
    }

 /*     * ***************Function logout**********************************
     * @type            : Function
     * @function name   : logout
     * @description     : Log Out the logged in user and redirected to Login page  
     * @param           : null 
     * @return          : null 
     * ********************************************************** */
    
    function logout(){
        $this->session->sess_destroy();
        $data = array();
        $data['page'] = 'logout';
        $this->load->view('admin/login', $data);
    }

    public function register(){
        $data = array();
        $data['page'] = 'Register';
        $this->load->view('admin/register', $data);
    }

    public function reg(){

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[user.email]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('errors', validation_errors());
            redirect('auth/register');
        }

        if($_POST){ 
            $query = $this->login_model->save_user();
            
            //-- if valid
            if($query){
                $this->session->set_flashdata('success', 'Success! Account created successfully');
                redirect(base_url() . 'auth', 'refresh');
            }else{
                $this->session->set_flashdata('errors', 'Something went wrong!');
                redirect(base_url() . 'auth/register', 'refresh');
            }
            
        }else{
            $this->load->view('auth',$data);
        }
    }

    public function activeAccount($email) {
        $email = base64_decode($email);
        $this->login_model->activeAccount($email);
        $this->session->set_flashdata('success', 'Success! Your account activated successfully!');
        redirect(base_url() . 'auth', 'refresh');
    }

    public function forgotPassword(){
        $data = array();
        $data['page'] = 'Forgot Password';
        $this->load->view('admin/forgot-password', $data);
    }

    public function sendForgotPasswordLink() {
        $this->login_model->sendForgotLink();
        $this->session->set_flashdata('success', 'Success! Check your mailbox to reset your password!');
        redirect(base_url() . 'auth/forgot-password', 'refresh');
    }

    public function resetPassword($email){
        $data = array();
        $email = base64_decode($email);
        $userInfo = $this->login_model->getUserByEmail($email);
        if(!empty($userInfo)) {
            $data['user_id'] = $userInfo->id;
            $data['page'] = 'Forgot Password';
            $this->load->view('admin/reset-password', $data);
        } else {
            $this->session->set_flashdata('error', 'Error! Something went wrong!');
            redirect(base_url() . 'auth', 'refresh');
        }
        
    }

    public function updatePassword() {
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('errors', validation_errors());
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->login_model->updatePassword();
        $this->session->set_flashdata('success', 'Success! Password updated!');
        redirect(base_url() . 'auth', 'refresh');
    }

}