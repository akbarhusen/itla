<?php
class Common_model extends CI_Model {

    //-- insert function
	public function insert($data,$table){
        $this->db->insert($table,$data);        
        return $this->db->insert_id();
    }

    //-- edit function
    function edit_option($action, $id, $table){
        $this->db->where('id',$id);
        $this->db->update($table,$action);
        return;
    } 

    //-- update function
    function update($action, $id, $table){
        $this->db->where('id',$id);
        $this->db->update($table,$action);
        return;
    } 

    //-- delete function
    function delete($id,$table){
        $this->db->delete($table, array('id' => $id));
        return;
    }

    //-- user role delete function
    function delete_user_role($id,$table){
        $this->db->delete($table, array('user_id' => $id));
        return;
    }


    //-- select function
    function select($table){
        $this->db->select();
        $this->db->from($table);
        $this->db->order_by('id','ASC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    //-- select by id
    function select_option($id,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    } 

    //-- select by id
    function getOne($id,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    } 

    //-- check user role power
    function check_power($type){
        $this->db->select('ur.*');
        $this->db->from('user_role ur');
        $this->db->where('ur.user_id', $this->session->userdata('id'));
        $this->db->where('ur.action', $type);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    public function check_email($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {                 
            return $query->result();
        }else{
            return false;
        }
    }

    public function check_exist_power($id){
        $this->db->select('*');
        $this->db->from('user_power');
        $this->db->where('power_id', $id); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {                 
            return $query->result();
        }else{
            return false;
        }
    }


    //-- get all power
    function get_all_power($table){
        $this->db->select();
        $this->db->from($table);
        $this->db->order_by('power_id','ASC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    //-- get logged user info
    function get_user_info(){
        $this->db->select('u.*');
        $this->db->select('c.name as country_name');
        $this->db->from('user u');
        $this->db->join('country c','c.id = u.country','LEFT');
        $this->db->where('u.id',$this->session->userdata('id'));
        $this->db->order_by('u.id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }

    //-- get single user info
    function get_single_user_info($id){
        $this->db->from('user u');
        $this->db->where('u.id',$id);
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }

    //-- get single user info
    function get_user_role($id){
        $this->db->select('ur.*');
        $this->db->from('user_role ur');
        $this->db->where('ur.user_id',$id);
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }


    //-- get all users with type 2
    function get_all_user(){
        $this->db->select('u.*');
        $this->db->from('user u');
        $this->db->order_by('u.id','DESC');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }


    //-- count active, inactive and total user
    function get_user_total(){
        $this->db->select('*');
        $this->db->select('count(*) as total');
        $this->db->select('(SELECT count(user.id)
                            FROM user 
                            WHERE (user.status = 1)
                            )
                            AS active_user',TRUE);

        $this->db->select('(SELECT count(user.id)
                            FROM user 
                            WHERE (user.status = 0)
                            )
                            AS inactive_user',TRUE);

        $this->db->select('(SELECT count(user.id)
                            FROM user 
                            WHERE (user.role = "admin")
                            )
                            AS admin',TRUE);

        $this->db->from('user');
        $query = $this->db->get();
        $query = $query->row();  
        return $query;
    }


    //-- image upload function with resize option
    function upload_image(){
            
            //-- set upload path
            $config['upload_path']  = "./uploads/animals/";
            $config['allowed_types']= 'gif|jpg|png|jpeg';
            $config['max_size']     = '92000';
            $config['max_width']    = '92000';
            $config['max_height']   = '92000';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload("image")) {

                
                $data = $this->upload->data();
                // //-- set upload path
                // $source             = $config['max_height']."".$data['file_name'] ;
                // $destination_thumb  = $config['max_height']."thumbnail/" ;
                // $destination_medium = $config['max_height']."medium/" ;
                // $main_img = $data['file_name'];
                // // Permission Configuration
                // // chmod($source, 0777) ;

                // /* Resizing Processing */
                // // Configuration Of Image Manipulation :: Static
                // $this->load->library('image_lib') ;
                // $img['image_library'] = 'GD2';
                // $img['create_thumb']  = TRUE;
                // $img['maintain_ratio']= TRUE;

                // /// Limit Width Resize
                // $limit_medium   = $max_size ;
                // $limit_thumb    = 200 ;

                // // Size Image Limit was using (LIMIT TOP)
                // $limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;

                // // Percentase Resize
                // if ($limit_use > $limit_medium || $limit_use > $limit_thumb) {
                //     $percent_medium = $limit_medium/$limit_use ;
                //     $percent_thumb  = $limit_thumb/$limit_use ;
                // }

                // //// Making THUMBNAIL ///////
                // $img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent_thumb : $data['image_width'] ;
                // $img['height'] = $limit_use > $limit_thumb ?  $data['image_height'] * $percent_thumb : $data['image_height'] ;

                // // Configuration Of Image Manipulation :: Dynamic
                // $img['thumb_marker'] = '_thumb-'.floor($img['width']).'x'.floor($img['height']) ;
                // $img['quality']      = ' 100%' ;
                // $img['source_image'] = $source ;
                // $img['new_image']    = $destination_thumb ;

                // $thumb_nail = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // // Do Resizing
                // $this->image_lib->initialize($img);
                // $this->image_lib->resize();
                // $this->image_lib->clear() ;

                // ////// Making MEDIUM /////////////
                // $img['width']   = $limit_use > $limit_medium ?  $data['image_width'] * $percent_medium : $data['image_width'] ;
                // $img['height']  = $limit_use > $limit_medium ?  $data['image_height'] * $percent_medium : $data['image_height'] ;

                // // Configuration Of Image Manipulation :: Dynamic
                // $img['thumb_marker'] = '_medium-'.floor($img['width']).'x'.floor($img['height']) ;
                // $img['quality']      = '100%' ;
                // $img['source_image'] = $source ;
                // $img['new_image']    = $destination_medium ;

                // $mid = $data['raw_name']. $img['thumb_marker'].$data['file_ext'];
                // // Do Resizing
                // $this->image_lib->initialize($img);
                // $this->image_lib->resize();
                // $this->image_lib->clear() ;

                // //-- set upload path
                // $images = $config['upload_path'].'medium/'.$mid;
                // $thumb  = $config['upload_path'].'thumbnail/'.$thumb_nail;

                // unlink($data['full_path']) ;

                // return array(
                //     'images' => $images,
                //     'thumb' => $thumb
                // );
                return $data;
            }
            else {
                return 'failed';
            }
            
    }

    function getMyusersAjx($postData=null){

     $response = array();

     ## Read value
     $draw = $postData['draw'];
     $start = $postData['start'];
     $rowperpage = $postData['length']; // Rows display per page
     $columnIndex = $postData['order'][0]['column']; // Column index
     $columnName = $postData['columns'][$columnIndex]['data']; // Column name
     $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
     $searchValue = $postData['search']['value']; // Search value

     ## Search 
     $searchQuery = "";
     if($searchValue != ''){
        $searchQuery = " (first_name like '%".$searchValue."%' or last_name like '%".$searchValue."%' or email like'%".$searchValue."%' or username like'%".$searchValue."%' ) ";
     }

     ## Total number of records without filtering
     $this->db->select('count(*) as allcount');
     
     $records = $this->db->get('user')->result();
     $totalRecords = $records[0]->allcount;

     ## Total number of record with filtering
     $this->db->select('count(*) as allcount');
     if($searchQuery != '')
        $this->db->where($searchQuery);
    
     $records = $this->db->get('user')->result();
     $totalRecordwithFilter = $records[0]->allcount;

     ## Fetch records
     $this->db->select('*');
     if($searchQuery != '')
        $this->db->where($searchQuery);
    
     $this->db->order_by($columnName, $columnSortOrder);
     $this->db->limit($rowperpage, $start);
     $records = $this->db->get('user')->result();

     $data = array();

     foreach($records as $key => $record ){

        $status = '';
        if ($record->status == 0):
            $status .= '<div class="label label-table label-danger">Inactive</div>';
        else:
            $status .= '<div class="label label-table label-success">Active</div>';
        endif;

        $role = '';
        if ($record->role == 'admin'):
            $role .= '<div class="label label-table label-info"><i class="fa fa-user"></i> admin</div>';
        else:
            $role .= '<div class="label label-table label-success">user</div>';
        endif;

        $action = '';
        if ($this->session->userdata('role') == 'admin'):
                                        
            $action .= '<a href="'.base_url('admin/user/update/'.$record->id).'"><button type="button" class="btn btn-info btn-circle btn-xs"><i class="fa fa-edit"></i></button></a>
                                                    
                <a href="javascript:void(0);" class="delete" data-delete_type="User" data-href="'.base_url('admin/user/delete/'.$record->id).'" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>';


            else:

                if(check_power(2)):

            $action .= '<a href="'.base_url('admin/user/update/'.$record->id).'"><button type="button" class="btn btn-success btn-circle btn-xs"><i class="fa fa-edit"></i></button></a>';

                endif;
                
                if(check_power(3)):
                
                    
            $action .= '<a href="javascript:void(0);" class="delete" data-delete_type="User" href="'.base_url('admin/user/delete/'.$record->id).'" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>';

                endif;

            endif;

            
            
            if ($record->status == 1):
                                                        
            $action .= ' <a href="javascript:void(0);" class="deativate" data-delete_type="User" data-href="'.base_url('admin/user/deactive/'.$record->id).'" data-toggle="tooltip" data-original-title="Deactive"><button type="button" class="btn btn-warning btn-circle btn-xs"><i class="fa fa-minus"></i></button></a>';
                
                
            else:

            $action .= '<a href="'.base_url('admin/user/active/'.$record->id).'" data-toggle="tooltip" data-original-title="Activate"><button type="button" class="btn btn-success btn-circle btn-xs"><i class="fa fa-check"></i></button></a>';
            endif;
        $email = !empty($record->email) ? $record->email : '';
        $data[] = array( 
           "username" => '<a href="'.base_url('admin/user/update/'.$record->id).'">'.$record->first_name." ".$record->last_name.'</a>',
           "email" => $email,
           "status" => $status,
           "role" => $role,
           "joining_date" => my_date_show_time($record->created_at),
           "action" => $action
        );
     }

     ## Response
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordwithFilter,
        "aaData" => $data
     );

     return $response; 
   }

   public function validMember($userEmail, $memberEmail) {
    $this->db->where('email', $memberEmail);
    $query = $this->db->get('members');
    $row = $query->row();
    if($row->email == $userEmail) {
        return array('status' => true, 'msg' => 'success', 'data' => $row);
    } else {
        return array('status' => false, 'msg' => 'Your email not matching with member.');
    }
   }

}