<?php
class Animal_model extends CI_Model {

    //-- insert function
	public function myAnimals(){
		$this->db->where('user_id', user()['id']);
        $this->db->where("animals.status", 1);
        $query = $this->db->get('animals');        
        return $query->result();
    }

    public function exportAnimals(){
        $this->db->where("animals.status", 1);
        $query = $this->db->get('animals');        
        return $query->result();
    }
    public function animals(){
    	$this->db->select('animals.*, animals.id as animal_id, claim_animals.id as claim_id, claim_animals.status as task_status');
    	$this->db->join('claim_animals', 'claim_animals.animal_id = animals.id', 'left');
        $this->db->where("animals.status", 1);
        $this->db->order_by("id", "desc");
        $query = $this->db->get('animals');
        return $query->result();
    }
    public function myTotalAnimals(){
		$this->db->where('user_id', user()['id']);
        $this->db->where("animals.status", 1);
        $query = $this->db->get('animals');        
        return $query->num_rows();
    }

    public function totalAnimals() {
        $this->db->where("animals.status", 1);
        $query = $this->db->get('animals');        
        return $query->num_rows();
    }
    public function getAnimalType($type){
        $type = ($type == 1) ? 'Bull' : 'Cow';
    	$term = $this->input->get('term');
    	$this->db->select('name as value, id as key');
		$this->db->where('sex', $type);
		$this->db->like('name', $term);
        $query = $this->db->get('animals');
        return json_encode($query->result());
    }

    public function getAnimalInfo($id){
		$this->db->where('id', $id);
		$query = $this->db->get('animals');
        return $query->row();
    }
    public function claim($id) {
    	$this->db->where('animal_id', $id);
        $query = $this->db->get('claim_animals');

        if($query->num_rows() > 0) {
        	return false;
        } else {
            $taskdata = array(
                        'user_id' => user()['id'],
                        'animal_id' => $id,
                        'action' => 'Claim animal',
                        'link' => base_url('/admin/animal/'.$id),
                        'status' => 0,
                        'extra_info' => serialize(array(
                                'request_from' => user()['name'],
                                'animal' => $this->getAnimalInfo($id)->name
                            )
                            ),
                        'created_at' => date('Y-m-d H:i:s')
                    );
            $taskdata = $this->security->xss_clean($taskdata);
            $this->common_model->insert($taskdata, 'claim_animals');

        	// $data = array(
         //        'user_id' => user()['id'],
         //        'animal_id' => $id,
         //        'action' => user()['first_name'].' claimed animal',
         //        'status' => 0,
         //        'created_at' => current_datetime()
         //    );

         //    $data = $this->security->xss_clean($data);
         //    $user_id = $this->common_model->insert($data, 'claim_animals');
            return true;
        }
    }

    public function tasks() {
    	$this->db->select('claim_animals.*, claim_animals.id as claim_id, claim_animals.created_at as task_date, claim_animals.status as task_status, user.first_name');
    	$this->db->join('user', 'user.id = claim_animals.user_id', 'left');
        $this->db->order_by('claim_animals.id', 'DESC');
        $query = $this->db->get('claim_animals');        
        return $query->result();
    }

    public function members(){
        $query = $this->db->get('members');        
        return $query->result();
    }

    function getAnimalsAjx($postData=null){

     $response = array();

     ## Read value
     $draw = $postData['draw'];
     $start = $postData['start'];
     $rowperpage = $postData['length']; // Rows display per page
     $columnIndex = $postData['order'][0]['column']; // Column index
     $columnName = $postData['columns'][$columnIndex]['data']; // Column name
     $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
     $searchValue = $postData['search']['value']; // Search value

     ## Search 
     $searchQuery = "";
     if($searchValue != ''){
        $searchQuery = " (name like '%".$searchValue."%' or id like '%".$searchValue."%' or tlba_number like'%".$searchValue."%' ) ";
     }

     ## Total number of records without filtering
     $this->db->select('count(*) as allcount');
     $records = $this->db->get('animals')->result();
     $totalRecords = $records[0]->allcount;

     ## Total number of record with filtering
     $this->db->select('count(*) as allcount');
     if($searchQuery != '')
        $this->db->where($searchQuery);
     $records = $this->db->get('animals')->result();
     $totalRecordwithFilter = $records[0]->allcount;

     ## Fetch records
     $this->db->select('*');
     if($searchQuery != '')
        $this->db->where($searchQuery);
     $this->db->order_by($columnName, $columnSortOrder);
     $this->db->limit($rowperpage, $start);
     $records = $this->db->get('animals')->result();

     $data = array();

     foreach($records as $key => $record ){

        $image = '';
        if(!empty($record->image)): 
            $imageLink = base_url()."uploads/animals/".$record->image;
        else:
            $imageLink = base_url()."optimum/images/default-image.png";
        endif;
        $image .= '<a href="'.base_url('admin/animal/'.$record->id).'">';
        $image .= '<img src="'.$imageLink.'" height="50"></a>';
        $animalSirInfo = animalInfo($record->sire);
        $animalDamInfo = animalInfo($record->dam);
        $sire_name = !empty($animalSirInfo->name) ? $animalSirInfo->name : '';
        $dam_name = !empty($animalDamInfo->name) ? $animalDamInfo->name : '';
        $data[] = array( 
           "image" => $image,
           "name" => '<a href="'.base_url('admin/animal/'.$record->id).'">'.$record->name.'</a>',
           "dob" => $record->dob,
           "sex" => $record->sex,
           "sire" => $sire_name,
           "dam" => $dam_name,
           "itla" => $record->itla,
           "tlba_number" => $record->tlba_number,
           "private_herd" => $record->private_herd,
           "private_herd_location" => $record->private_herd_location,
           "user" => !empty($animal->user_id) ? $this->common_model->select_option($animal->user_id, 'user')[0]['first_name'] : '',
           "type" => $record->type,
           "action" => ""
        ); 
         $action_link = '';
        if ($this->session->userdata('role') == 'admin'):
            $action_link .= '<a href="'.base_url('user/animal/edit/'.$record->id).'"><button type="button" class="btn btn-info btn-circle btn-xs" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button></a>';

            $action_link .= '<a href="javascript:void(0);" data-href="'.base_url('user/animal/delete/'.$record->id).'" class="delete" data-delete_type="animals" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>';
            
        else: 
        if($record->user_id != $this->session->userdata('id')): 
            if($record->user_id == 0): 
                if(empty($record->claim_id)): 
                    $action_link .= '<a href="'.base_url('user/animal/claim/'.$record->id).'"><button type="button" class="btn btn-info btn-circle btn-xs" data-toggle="tooltip" data-original-title="Claim Animal"><i class="fa fa-file"></i></button></a>';
                else: 
                    if($record->task_status == 1): 
                        $action_link .= '<label class="label label-table label-success">Claimed</label>';
                    else: 
                        $action_link .= '<label class="label label-table label-warning">Request Sent</label>';
                    endif; 
                endif;         
                endif;         
            endif; 
        endif;
        $data[$key]["action"] = $action_link;
     }

     ## Response
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordwithFilter,
        "aaData" => $data
     );

     return $response; 
   }

   function getMyAnimalsAjx($postData=null){

     $response = array();

     ## Read value
     $draw = $postData['draw'];
     $start = $postData['start'];
     $rowperpage = $postData['length']; // Rows display per page
     $columnIndex = $postData['order'][0]['column']; // Column index
     $columnName = $postData['columns'][$columnIndex]['data']; // Column name
     $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
     $searchValue = $postData['search']['value']; // Search value

     ## Search 
     $searchQuery = "";
     if($searchValue != ''){
        $searchQuery = " (name like '%".$searchValue."%' or id like '%".$searchValue."%' or tlba_number like'%".$searchValue."%' ) ";
     }

     ## Total number of records without filtering
     $this->db->select('count(*) as allcount');
     $this->db->where('user_id', $this->session->userdata('id'));
     $records = $this->db->get('animals')->result();
     $totalRecords = $records[0]->allcount;

     ## Total number of record with filtering
     $this->db->select('count(*) as allcount');
     if($searchQuery != '')
        $this->db->where($searchQuery);
    $this->db->where('user_id', $this->session->userdata('id'));
     $records = $this->db->get('animals')->result();
     $totalRecordwithFilter = $records[0]->allcount;

     ## Fetch records
     $this->db->select('*');
     if($searchQuery != '')
        $this->db->where($searchQuery);
    $this->db->where('user_id', $this->session->userdata('id'));
     $this->db->order_by($columnName, $columnSortOrder);
     $this->db->limit($rowperpage, $start);
     $records = $this->db->get('animals')->result();

     $data = array();

     foreach($records as $key => $record ){

        $image = '';
        if(!empty($record->image)): 
            $imageLink = base_url()."uploads/animals/".$record->image;
        else:
            $imageLink = base_url()."optimum/images/default-image.png";
        endif;
        $image .= '<a href="'.base_url('admin/animal/'.$record->id).'">';
        $image .= '<img src="'.$imageLink.'" height="50"></a>';
        $animalSirInfo = animalInfo($record->sire);
        $animalDamInfo = animalInfo($record->dam);
        $sire_name = !empty($animalSirInfo->name) ? $animalSirInfo->name : '';
        $dam_name = !empty($animalDamInfo->name) ? $animalDamInfo->name : '';
        $data[] = array( 
           "image" => $image,
           "name" => '<a href="'.base_url('admin/animal/'.$record->id).'">'.$record->name.'</a>',
           "dob" => $record->dob,
           "sex" => $record->sex,
           "sire" => $sire_name,
           "dam" => $dam_name,
           "itla" => $record->itla,
           "tlba_number" => $record->tlba_number,
           "private_herd" => $record->private_herd,
           "private_herd_location" => $record->private_herd_location,
           "user" => !empty($animal->user_id) ? $this->common_model->select_option($animal->user_id, 'user')[0]['first_name'] : '',
           "type" => $record->type,
           "status" => ($record->status == 1) ? '<div class="label label-table label-success">Approved</div>' : '<div class="label label-table label-warning">Pending</div>',
           "action" => ""
        ); 
        
        $action_link = '';
        
        $action_link .= '<a href="'.base_url('user/animal/edit/'.$record->id).'"><button type="button" class="btn btn-info btn-circle btn-xs" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button></a>';

        $action_link .= '<a href="javascript:void(0);" data-href="'.base_url('user/animal/delete/'.$record->id).'" class="delete" data-delete_type="animals" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>';
            
        
        $data[$key]["action"] = $action_link;
     }

     ## Response
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordwithFilter,
        "aaData" => $data
     );

     return $response; 
   }

}