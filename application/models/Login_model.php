<?php
class Login_model extends CI_Model {

    public function edit_option_md5($action, $id, $table){
        $this->db->where('md5(id)',$id);
        $this->db->update($table,$action);
        return;
    }

    //-- check post email
    public function check_email($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {                 
            return $query->result();
        }else{
            return false;
        }
    }


    // check valid user by id
    public function validate_id($id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('md5(id)', $id); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1){                 
            return $query->result();
        }
        else{
            return false;
        }
    }



    //-- check valid user
    function validate_user(){            
        
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $this->input->post('user_name')); 
        $this->db->where('password', md5($this->input->post('password')));
        $this->db->where('status', 1);
        $this->db->limit(1);
        $query = $this->db->get();   
        
        if($query->num_rows() == 1){                 
           return $query->result();
        }
        else{
            return false;
        }
    }

    //-- check valid user
    function save_user(){            
        
        $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
                'created_at' => date('Y-m-d H:i:s'),
                'status' => 1,
                'role' => 'user'
        );

        if($this->db->insert('user', $data)) {

            // $message = "<h3>Thanks for registered in ITLA</h3><br>";
            // $message .= "<a href=".base_url('auth/verify-account/'.base64_encode($this->input->post('email')).'').">Click here</a> to verify your account.<br>";
            // $message .= "<br>Thanks,<br>ITLA";
            // sendEmail($this->input->post('email'), 'Verify Account - ITLA', $message);
            return true;
        } else {
            return false;
        }
    }

    public function activeAccount($email) {
        $data = array('status' => 1);
        $this->db->where('email', $email);
        $this->db->update('user', $data);
    }

    public function sendForgotLink() {
        $message = "<h3>Thanks for being a part of ITLA</h3><br>";
        $message .= "<a href=".base_url('auth/reset-password/'.base64_encode($this->input->post('email')).'').">Click here</a> to reset your password.<br>";
        $message .= "<br>Thanks,<br>ITLA";
        sendEmail($this->input->post('email'), 'Reset Password - ITLA', $message);
    }

    public function getUserByEmail($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('user');
        return $query->row();
    }

    public function updatePassword() {
        $data = array('password' => md5($this->input->post('password')));
        $this->db->where('id', $this->input->post('user_id'));
        $this->db->update('user', $data);
    }

}