<?php
class Member_model extends CI_Model {

    function getMembersAjx($postData=null){

     $response = array();

     ## Read value
     $draw = $postData['draw'];
     $start = $postData['start'];
     $rowperpage = $postData['length']; // Rows display per page
     $columnIndex = $postData['order'][0]['column']; // Column index
     $columnName = $postData['columns'][$columnIndex]['data']; // Column name
     $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
     $searchValue = $postData['search']['value']; // Search value

     ## Search 
     $searchQuery = "";
     if($searchValue != ''){
        $searchQuery = " (member like '%".$searchValue."%' or membership_type like '%".$searchValue."%' or address like'%".$searchValue."%' or email like'%".$searchValue."%' ) ";
     }

     ## Total number of records without filtering
     $this->db->select('count(*) as allcount');
     
     $records = $this->db->get('members')->result();
     $totalRecords = $records[0]->allcount;

     ## Total number of record with filtering
     $this->db->select('count(*) as allcount');
     if($searchQuery != '')
        $this->db->where($searchQuery);
    
     $records = $this->db->get('members')->result();
     $totalRecordwithFilter = $records[0]->allcount;

     ## Fetch records
     $this->db->select('*');
     if($searchQuery != '')
        $this->db->where($searchQuery);
    
     $this->db->order_by($columnName, $columnSortOrder);
     $this->db->limit($rowperpage, $start);
     $records = $this->db->get('members')->result();

     $data = array();

     foreach($records as $key => $record ){

        $action = '';
        if ($this->session->userdata('role') == 'admin'):
            $action .= '<a href="'.base_url('admin/member/edit/'.$record->id).'"><button type="button" class="btn btn-info btn-circle btn-xs"><i class="fa fa-edit"></i></button></a> ';
            $action .= '<a href="javascript:void(0);" data-href="'.base_url('admin/member/delete/'.$record->id).'" class="delete" data-delete_type="Member" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>';
        endif;
        $data[] = array( 
           "id" => '<a href="'.base_url('admin/member/single/'.$record->id.'').'">'.$record->id.'</a>',
           "member" => '<a href="'.base_url('admin/member/single/'.$record->id.'').'">'.$record->member.'</a>',
           "membership_type" => $record->membership_type,
           "address" => $record->address,
           "email" => $record->email,
           "renewal_date" => my_date_show_time($record->renewal_date),
           "action" => $action
        );
     }

     ## Response
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordwithFilter,
        "aaData" => $data
     );

     return $response; 
   }

   public function getMembersAutoComplete() {
        $term = $this->input->get('term');
        $this->db->select('member as value, id as key, email');
        $this->db->like('member', $term);
        $query = $this->db->get('members');
        return json_encode($query->result());
   }

}