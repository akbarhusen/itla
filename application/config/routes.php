<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['auth/register'] = 'auth/register';
$route['auth/forgot-password'] = 'auth/forgotPassword';
$route['auth/verify-account/(:any)'] = 'auth/activeAccount/$1';
$route['auth/reset-password/(:any)'] = 'auth/resetPassword/$1';


$route['admin/animals'] = 'admin/Dashboard/index';
$route['admin/tasks'] = 'admin/Tasks/index';
$route['admin/members'] = 'admin/Members/index';
$route['admin/members/add'] = 'admin/Members/add';
$route['admin/members/import_animal'] = 'admin/Members/import_animal';
$route['admin/member/import_animal_csv'] = 'admin/Members/import_animal_csv';
$route['admin/member/edit/(:any)'] = 'admin/Members/edit/$1';
$route['admin/member/delete/(:any)'] = 'admin/Members/delete/$1';
$route['admin/member/save'] = 'admin/Members/save';
$route['admin/member/update'] = 'admin/Members/update';
$route['admin/member/single/(:any)'] = 'admin/Members/single/$1';
$route['admin/tasks/approve-claim/(:any)/(:any)/(:any)'] = 'admin/Tasks/approveClaim/$1/$2/$3';
$route['admin/tasks/reject-claim/(:any)'] = 'admin/Tasks/rejectClaim/$1';
$route['admin/animal/(:any)'] = 'admin/Dashboard/any/$1';
$route['admin/animals/export/csv'] = 'admin/Dashboard/exportCsv';
$route['user/my-animals/get-members'] = 'admin/Members/getMembersAutoComplete';


$route['user/my-animals'] = 'user/MyAnimals/index';
$route['user/my-animals/create/(:any)'] = 'user/MyAnimals/create/$1';
$route['user/my-animals/save'] = 'user/MyAnimals/save';
$route['user/my-animals/get-animal-type/(:any)'] = 'user/MyAnimals/getAnimalType/$1';
$route['user/animal/edit/(:any)'] = 'user/MyAnimals/edit/$1';
$route['user/animal/delete/(:any)'] = 'user/MyAnimals/delete/$1';
$route['user/animal/claim/(:any)'] = 'user/MyAnimals/claim/$1';
$route['user/membership-directory'] = 'user/MembershipDirectory/index';
$route['user/shopping-cart'] = 'user/ShoppingCart/index';
$route['register-animal'] = 'user/MyAnimals/registerAnimal';
$route['user/my-animals/update-status/(:any)/(:any)'] = 'user/MyAnimals/updateStatus/$1/$2';
$route['user/my-animals/export/csv'] = 'user/MyAnimals/exportCsv';



$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
