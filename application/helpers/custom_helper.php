<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 	
	//-- check logged user
	if (!function_exists('check_login_user')) {
	    function check_login_user() {
	        $ci = get_instance();
	        if ($ci->session->userdata('is_login') != TRUE) {
	            $ci->session->sess_destroy();
	            redirect(base_url('auth'));
	        }
	    }
	}

	if (!function_exists('user')) {
	    function user() {
	        $ci = get_instance();
	        return $ci->session->userdata();
	    }
	}

	if(!function_exists('check_power')){
	    function check_power($type){        
	        $ci = get_instance();
	        
	        $ci->load->model('common_model');
	        $option = $ci->common_model->check_power($type);        
	        
	        return $option;
	    }
    } 

	//-- current date time function
	if(!function_exists('current_datetime')){
	    function current_datetime(){        
	        $dt = new DateTime('now', new DateTimezone('Asia/KOLKATA'));
	        $date_time = $dt->format('Y-m-d H:i:s');      
	        return $date_time;
	    }
	}

	//-- show current date & time with custom format
	if(!function_exists('my_date_show_time')){
	    function my_date_show_time($date){       
	        if($date != ''){
	            $date2 = date_create($date);
	            $date_new = date_format($date2,"d M Y h:i A");
	            return $date_new;
	        }else{
	            return '';
	        }
	    }
	}

	//-- show current date with custom format
	if(!function_exists('my_date_show')){
	    function my_date_show($date){       
	        
	        if($date != ''){
	            $date2 = date_create($date);
	            $date_new = date_format($date2,"d M Y");
	            return $date_new;
	        }else{
	            return '';
	        }
	    }
	}

	//-- show current date with custom format
	if(!function_exists('sendEmail')){
	    function sendEmail($toEmail, $subject, $message){    
	        $ci = get_instance();
	        $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'akbarmaknojiya@gmail.com',
                'smtp_pass' => 'pmdlqfzmupfvwjwc',
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );
            $ci->load->library('email');

            $ci->email->initialize($config);

            $ci->email->from('info@itla.com', 'ITLA'); 
            $ci->email->to($toEmail);
            $ci->email->subject($subject);
            $ci->email->message($message); 
            $ci->email->send();
	    }
	}


	if(!function_exists('dd')){
	    function dd($data){    
	        echo "<pre>";
	        print_r($data);
	        exit();
	    }
	}

	if(!function_exists('animalInfo')){
	    function animalInfo($id){    
	        $CI =& get_instance();
	        $CI->load->database();

	        $CI->db->where('id', $id);
	        $query = $CI->db->get('animals');
	        return $query->row();
	    }
	}

  